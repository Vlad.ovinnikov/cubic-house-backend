"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var profile = exports.profile = {
  "Profile has been successfully updated": {
    "pl": "Profile has been successfully updated"
  },
  "Profile not found": {
    "pl": "Profil nie został odnaleziony"
  }
};