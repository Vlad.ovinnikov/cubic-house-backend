'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.profileLocal = exports.commentLocal = exports.categoryLocal = exports.entryLocal = exports.userLocal = undefined;

var _localize = require('localize');

var _localize2 = _interopRequireDefault(_localize);

var _user = require('./user.lang');

var _entry = require('./entry.lang');

var _category = require('./category.lang');

var _comment = require('./comment.lang');

var _profile = require('./profile.lang');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var userLocal = exports.userLocal = new _localize2.default(_user.user);
var entryLocal = exports.entryLocal = new _localize2.default(_entry.entry);
var categoryLocal = exports.categoryLocal = new _localize2.default(_category.category);
var commentLocal = exports.commentLocal = new _localize2.default(_comment.comment);
var profileLocal = exports.profileLocal = new _localize2.default(_profile.profile);