'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
//-- Exports --//
var USER_ROLE = exports.USER_ROLE = {
  USER: 'USER',
  ADMIN: 'ADMIN',
  SUPER_ADMIN: 'SUPER_ADMIN'
};
// salt factor
var SALT_WORK_FACTOR = exports.SALT_WORK_FACTOR = 10;
// validation errors
var ERRORS = exports.ERRORS = {
  MIN: 'minlength',
  MAX: 'maxlength',
  REQUIRED: 'required',
  VALIDATOR: 'ValidatorError'
};

var PRIORITY = exports.PRIORITY = {
  NONE: 0,
  LOW: 1,
  HIGH: 2
};

var STATUS = exports.STATUS = {
  DRAFT: 0,
  PUBLISHED: 1,
  ARCHIVED: 2
};

var AUTH_HEADER = exports.AUTH_HEADER = 'x-auth';
var LANG_HEADER = exports.LANG_HEADER = 'x-lang';