'use strict';

var _config = require('./config');

//-- Implementation --//
// Set environment for testing db
var env = process.env.NODE_ENV || 'development'; //-- Dependencies --//


if (env === 'development' || env === 'test') {
  var envConfig = _config.configs[env];

  Object.keys(envConfig).forEach(function (key) {
    process.env[key] = envConfig[key];
  });
}