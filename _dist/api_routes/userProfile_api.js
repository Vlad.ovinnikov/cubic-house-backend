'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _index = require('./../models/_index');

var _translations = require('./../assets/translations/translations');

var _constants = require('./../configs/constants.config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * get user profile
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 */
function get(req, res) {
  var user = req.user;
  _index.UserProfile.findOne({ _userId: user._id }).then(function (profile) {
    res.status(200).send({ profile: profile, user: user });
  }).catch(function (err) {
    res.send(err);
  });
}
// app dependencies
//-- Dependencies --//
//-- lib dependencies


function update(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.profileLocal.setLocale(lang);

  var body = _underscore2.default.pick(req.body, ['firstName', 'lastName', 'avatar', 'gender', 'birthdate']);

  if (body.birthdate) {
    body.birthdate = (0, _moment2.default)(new Date(body.birthdate));
  }

  _index.UserProfile.findOneAndUpdate({ _userId: req.user._id }, { $set: body }, { new: true }).then(function (profile) {
    if (!profile) {
      return res.status(400).send({ message: _translations.profileLocal.translate('Profile not found') });
    }

    res.status(200).send({ profile: profile, message: _translations.profileLocal.translate('Profile has been successfully updated') });
  });
}

exports.default = { get: get, update: update };