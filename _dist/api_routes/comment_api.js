'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _mongodb = require('mongodb');

var _async = require('async');

var _index = require('./../models/_index');

var _translations = require('./../assets/translations/translations');

var _constants = require('./../configs/constants.config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * add new commment
 * @param {[type]} req [request param]
 * @param {[type]} res [response param]
 */
//-- Dependencies --//
// lib dependencies
function add(req, res, next) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.commentLocal.setLocale(lang);

  var body = _underscore2.default.pick(req.body, ['text', 'parentId', 'vote', 'entry']),
      commentBody = new _index.Comment({
    text: body.text,
    entry: body.entry
  });

  // check if entry id is valiud
  _checkId(res, commentBody.entry, 'There is no entry');
  // check if parent comment id is valid
  if (body.parentId) {
    commentBody.parentId = body.parentId;
    _checkId(res, commentBody.parentId, 'There is no comment');
  }

  if (!commentBody.text) {
    return res.status(400).send({ message: _translations.commentLocal.translate('Text is required') });
  }

  (0, _async.waterfall)([
  // check if entry exists
  function (done) {
    _index.Entry.findOne({ _id: commentBody.entry }).then(function (entry) {
      if (!entry) {
        return res.status(404).send({ message: _translations.commentLocal.translate('There is no entry') });
      }
      done(null, entry);
    }).catch(function (err) {
      return done(err);
    });
  },
  // find user profile by its id
  function (entry, done) {
    _index.UserProfile.findOne({ _userId: req.user._id }).then(function (userProfile) {

      commentBody.author = userProfile._id;
      done(null, commentBody, entry);
    }).catch(function (err) {
      return done(err);
    });
  },
  // save new comment to db
  function (comment, entry, done) {
    comment.save().then(function (newComment) {

      done(null, newComment, entry);
    }).catch(function (err) {
      return done(err);
    });
  }, function (comment, entry, done) {

    if (comment.parentId) {
      _index.Comment.findOne({ _id: comment.parentId }).then(function (parentComment) {

        if (!parentComment) {
          return res.status(404).send({ message: _translations.commentLocal.translate('There is no comment') });
        }

        parentComment.children.push(comment._id);
        parentComment.save().then(function () {
          done(null, comment, entry);
        }).catch(function (err) {
          return done(err);
        });
      });
    } else {
      done(null, comment, entry);
    }
  },
  // find entry by id and save new comment
  function (comment, entry, done) {

    _index.Entry.findOneAndUpdate({ _id: entry._id }, { $push: { comments: comment._id } }).then(function (data) {
      res.status(200).send(comment);
    }).catch(function (err) {
      return done(err);
    });
  }], function (err) {
    res.send(err);
  });
}

/**
 * get list comments by entry id
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */

// app dependencies
// import Comment from './../models/comment'
function getListWithEntryId(req, res) {
  var entryId = req.params.id;

  _checkId(res, entryId, 'There is no entry');

  _index.Comment.find({ entry: entryId, parentId: null }).populate('author children').then(function (comments) {
    var options = {
      path: 'children.author',
      model: 'UserProfile'
    };
    _index.Comment.populate(comments, options).then(function (popComments) {
      res.status(200).send(popComments);
    }).catch(function (err) {
      return res.send(err);
    });
  }).catch(function (err) {
    return res.send(err);
  });
}

/**
 * get 5 recent comments
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of comments or error]
 */
function getRecent(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.commentLocal.setLocale(lang);

  _index.Comment.find().sort({ updatedAt: 'desc' }).limit(5).populate('author entry').then(function (comments) {
    res.send(comments);
  }).catch(function (err) {
    res.send(err);
  });
}

/**
 * update comment
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
function update(req, res) {
  var commentId = req.params.id;
  var body = _underscore2.default.pick(req.body, ['text', 'author']);

  _checkId(res, commentId, 'There is no comment');

  _index.Comment.findOneAndUpdate({ _id: commentId, author: body.author._id }, { $set: body }, { new: true }).then(function (comment) {

    if (!comment) {
      return res.status(404).send({ message: _translations.commentLocal.translate('Comment is not found') });
    }

    res.send(comment);
  }).catch(function (err) {
    return res.send(err);
  });
}

/**
 * delete comment
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
function remove(req, res) {
  var commentId = req.params.commentId,
      entryId = req.params.entryId;

  _checkId(res, commentId, 'There is no comment');

  (0, _async.waterfall)([
  // finde user profile
  function (done) {
    _index.UserProfile.findOne({ _userId: req.user._id }).then(function (profile) {

      done(null, profile);
    }).catch(function (err) {
      return done(err);
    });
  },
  // remove comment (or parent comment)
  function (profile, done) {
    var children = [];
    _index.Comment.findOne({ _id: commentId, author: profile._id }).then(function (comment) {

      children = comment.children;
      children.push(comment._id);

      comment.remove().then(function () {
        done(null, children);
      }).catch(function (err) {
        return done(err);
      });
    }).catch(function (err) {
      return done(err);
    });
  }, function (comments, done) {

    _index.Entry.findOneAndUpdate({ _id: entryId }, { $pull: { comments: { $in: comments } } }).then(function (entry) {
      if (!entry) {
        return res.status(404).send({ message: entryLocal.translate('Entry is not found') });
      }

      res.status(200).send({ message: _translations.commentLocal.translate('Comment has been successfully removed') });
    }).catch(function (err) {
      return done(err);
    });
  }], function (err) {
    res.send(err);
  });
}

function _checkId(res, id, message) {
  if (!_mongodb.ObjectID.isValid(id)) {
    return res.status(404).send({ message: _translations.commentLocal.translate(message) });
  }
}

exports.default = { add: add, getListWithEntryId: getListWithEntryId, getRecent: getRecent, update: update, remove: remove };