'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _mongodb = require('mongodb');

var _async = require('async');

var _index = require('./../models/_index');

var _translations = require('./../assets/translations/translations');

var _constants = require('./../configs/constants.config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * add new category only can do admin or superAdmin
 * @param {[type]} req [request param]
 * @param {[type]} res [response param]
 */
//-- Dependencies --//
// lib dependencies
function add(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.categoryLocal.setLocale(lang);

  var body = _underscore2.default.pick(req.body, ['title', 'description']),
      category = new _index.Category({ title: body.title, description: body.description });

  if (!category.title) {
    return res.status(400).send({ message: _translations.categoryLocal.translate('Title is required') });
  }

  (0, _async.waterfall)([function (done) {
    _index.UserProfile.findOne({ _userId: req.user._id }).then(function (userProfile) {
      category.author = userProfile._id;
      done(null, userProfile);
    }).catch(function (err) {
      return done(err);
    });
  }, function (userProfile, done) {
    _index.Category.findOne({ title: category.title }).then(function (existingCategory) {

      if (existingCategory) {
        return res.status(400).send({ 'message': _translations.categoryLocal.translate('Category is already exists') });
      }
      done();
    }).catch(function (err) {
      return done(err);
    });
  }, function (done) {

    category.save().then(function (data) {
      data.populate('author', function (err) {
        if (err) {
          return res.status(400).send(err);
        }

        res.status(200).send(data);
      });
    }).catch(function (err) {
      res.status(400).send(err);
    });
  }], function (err) {
    res.send(err);
  });
}

/**
 * get list of categories
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of categories or error]
 */

// app dependencies
function getList(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.categoryLocal.setLocale(lang);

  _index.Category.find().sort({ 'title': 'asc' }).populate('author').then(function (categories) {

    if (!categories) {
      return res.status(404).send({ 'message': _translations.categoryLocal.translate('No categories. Please, add new one') });
    }

    res.status(200).send(categories);
  }).catch(function (err) {
    res.send(err);
  });
}

/**
 * get category by its id
 * @param  {[type]} req [reqiest param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [if id is valid and no another error return category]
 */
function get(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.categoryLocal.setLocale(lang);

  var id = req.params.id;

  if (!_mongodb.ObjectID.isValid(id)) {
    return res.status(404).send({ 'message': _translations.categoryLocal.translate('The requested category id is not correct') });
  }

  _index.Category.findOne({ _id: id }).populate('author').then(function (category) {
    if (!category) {
      return res.status(404).send({ 'message': _translations.categoryLocal.translate('The requested category does not exist') });
    }
    res.send(category);
  }).catch(function (err) {
    res.status(400).send(err);
  });
}

/**
 * update category by id
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [if no error returns updated category]
 */
function update(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.categoryLocal.setLocale(lang);

  var id = req.params.id,
      body = _underscore2.default.pick(req.body, ['title', 'description']);

  if (!_mongodb.ObjectID.isValid(id)) {
    return res.status(404).send({ 'message': _translations.categoryLocal.translate('The requested category id is not correct') });
  }

  _index.Category.findByIdAndUpdate(id, { $set: body }, { new: true }).populate('author').then(function (category) {
    if (!category) {
      return res.status(404).send({ 'message': _translations.categoryLocal.translate('The requested category does not exist') });
    }

    res.status(200).send(category);
  }).catch(function (err) {
    res.status(400).send(err);
  });
}

/**
 * remove entry
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [if no error return removed entry]
 */
function remove(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.categoryLocal.setLocale(lang);

  var id = req.params.id;

  if (!_mongodb.ObjectID.isValid(id)) {
    return res.status(404).send({ 'message': _translations.categoryLocal.translate('The requested category id is not correct') });
  }

  (0, _async.waterfall)([function (done) {
    _index.Category.findOne({ _id: id }).then(function (category) {
      if (!category) {
        return res.status(404).send({ message: _translations.categoryLocal.translate('The requested category does not exist') });
      }
      done(null, category);
    }).catch(function (err) {
      return done(err);
    });
  }, function (category, done) {
    category.remove().then(function (data) {
      res.status(200).send({ message: _translations.categoryLocal.translate('The category has successfully removed') });
    }).catch(function (err) {
      return res.send(err);
    });
  }], function (err) {
    res.send(err);
  });
}

//-- Exports --//
exports.default = { add: add, getList: getList, get: get, update: update, remove: remove };