'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _mongodb = require('mongodb');

var _async = require('async');

var _index = require('./../models/_index');

var _constants = require('./../configs/constants.config');

var _translations = require('./../assets/translations/translations');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * add new entry with category's and users id
 * @param {[type]} req [request param]
 * @param {[type]} res [response param]
 */


//-- app dependencies
function add(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.entryLocal.setLocale(lang);

  var body = _underscore2.default.pick(req.body, ['title', 'description', 'priority', 'status', 'category']),
      entry = new _index.Entry({
    title: body.title,
    description: body.description,
    priority: body.priority,
    status: body.status,
    category: body.category
  });

  if (!entry.title) {
    return res.status(400).send({ message: _translations.entryLocal.translate('Title is required') });
  }

  _index.Entry.findOne({ title: entry.title }).then(function (existingEntry) {
    if (existingEntry) {
      return res.status(400).send({ message: _translations.entryLocal.translate('Post with that title already exists') });
    } else {
      _checkId(res, req.user._id, 'User is not found');

      _index.UserProfile.findOne({ _userId: req.user._id }).then(function (profile) {

        entry.author = profile._id;
        entry.save().then(function (data) {
          res.send({
            message: _translations.entryLocal.translate('You have been successfully added new post'),
            entry: data
          });
        }).catch(function (err) {
          if (err.errors) {
            if (err.errors['description'].properties.type === _constants.ERRORS.REQUIRED) {
              return res.status(400).send({ message: _translations.entryLocal.translate('Description is required') });
            } else if (err.errors['description'].properties.type === _constants.ERRORS.MIN) {
              return res.status(400).send({ message: _translations.entryLocal.translate('Description is shorter than the minimum allowed length 10') });
            } else if (err.errors['title'].properties.type === _constants.ERRORS.MIN) {
              return res.status(400).send({ message: _translations.entryLocal.translate('Title is shorter than the minimum allowed length 3') });
            } else {
              res.status(400).send(err);
            }
          } else {
            return res.status(400).send(err);
          }
        });
      });
    }
  });
}

/**
 * get list of entries with category id
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of entries or error]
 */
//-- Dependencies --//
//-- lib dependencies
function getListWithCategory(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.entryLocal.setLocale(lang);

  var status = 1;
  var category = req.params.id;
  var perPage = parseInt(req.query.perPage, 10);
  var page = req.query.page > 0 ? parseInt(req.query.page, 10) : 0;

  _checkId(res, category, 'Category is not found');

  _index.Entry.find({ category: category, status: status }).limit(perPage).skip(perPage * page).sort({ updatedAt: 'desc' }).populate('author category').then(function (entries) {
    if (!entries || !entries.length) {
      return res.status(404).send({ message: _translations.entryLocal.translate('There are no posts in this category') });
    }

    _index.Entry.find().count(function (err, count) {
      var pages = Math.round(count) / perPage;
      res.send({ entries: entries, page: page, pages: pages });
    });
  }).catch(function (err) {
    res.send(err);
  });
}

/**
 * get list of entries 10 by page
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of entries or error]
 */
function getList(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.entryLocal.setLocale(lang);

  var perPage = parseInt(req.query.perPage, 10);
  var page = req.query.page > 0 ? parseInt(req.query.page, 10) : 0;

  _index.Entry.find().limit(perPage).skip(perPage * page).sort({ updatedAt: 'desc' }).populate('author category comments').then(function (entries) {

    _index.Entry.find().count(function (err, count) {
      var pages = Math.round(count) / perPage;
      res.send({ entries: entries, page: page, pages: pages });
    });
  }).catch(function (err) {
    res.send(err);
  });
}

/**
 * get list of entries posted by user
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of entries or error]
 */
function getUsersList(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.entryLocal.setLocale(lang);

  var perPage = parseInt(req.query.perPage, 10);
  var page = req.query.page > 0 ? parseInt(req.query.page, 10) : 0;
  var category = req.query.category;

  (0, _async.waterfall)([function (done) {
    _index.UserProfile.findOne({ _userId: req.user._id }).then(function (profile) {
      done(null, profile);
    }).catch(function (err) {
      return done(err);
    });
  }, function (profile, done) {
    if (category) {
      _checkId(res, category, 'Category is not found');
      _index.Entry.find({ author: profile._id, category: category }).limit(perPage).skip(perPage * page).sort({ updatedAt: 'desc' }).populate('author category comments').then(function (entries) {
        done(null, entries);
      }).catch(function (err) {
        done(err);
      });
    } else {
      _index.Entry.find({ author: profile._id }).limit(perPage).skip(perPage * page).sort({ updatedAt: 'desc' }).populate('author category comments').then(function (entries) {
        done(null, entries);
      }).catch(function (err) {
        done(err);
      });
    }
  }, function (entries, done) {
    _index.Entry.find().count(function (err, count) {
      var pages = Math.round(count) / perPage;
      if (!entries || !entries.length) {
        return res.status(404).send({ message: _translations.entryLocal.translate('There are no posts in this category') });
      }
      res.status(200).send({ entries: entries, page: page, pages: pages });
    });
  }], function (err) {
    res.send(err);
  });
}

/**
 * get 5 recent entries
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of entries or error]
 */
function getRecent(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.entryLocal.setLocale(lang);

  _index.Entry.find().sort({ updatedAt: 'desc' }).limit(5).populate('author category comments').then(function (entries) {
    res.send(entries);
  }).catch(function (err) {
    res.send(err);
  });
}

/**
 * get entry by its id
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return entry or error]
 */
function get(req, res) {
  var id = req.params.id;

  _checkId(res, id, 'Entry is not found');

  _index.Entry.findById(id).populate('author category').then(function (entry) {
    if (!entry) {
      return res.status(404).send({ message: _translations.entryLocal.translate('Entry is not found') });
    }

    res.send(entry);
  }).catch(function (err) {
    res.send(err);
  });
}

/**
 * update entry by its id and only owner can do it
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return entry if no error]
 */
function update(req, res) {
  var id = req.params.id,
      body = _underscore2.default.pick(req.body, ['title', 'description', 'priority', 'status', 'category', 'author']);

  _checkId(res, id, 'Entry is not found');

  _index.Entry.findOneAndUpdate({ _id: id, author: body.author._id }, { $set: body }, { new: true }).then(function (entry) {
    if (!entry) {
      return res.status(404).send({ message: _translations.entryLocal.translate('Entry is not found') });
    }

    res.send(entry);
  }).catch(function (err) {
    return res.send(err);
  });
}

/**
 * remove entry by its id and only owner can do it
 * @param  {[type]} req [requset param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [description]
 */
function remove(req, res) {
  var id = req.params.id;

  _checkId(res, id, 'Entry is not found');

  (0, _async.waterfall)([function (done) {
    _index.UserProfile.findOne({ _userId: req.user._id }).then(function (profile) {
      done(null, profile);
    }).catch(function (err) {
      return done(err);
    });
  }, function (profile, done) {
    _index.Entry.findOne({ _id: id, author: profile._id }).then(function (entry) {
      if (!entry) {
        return res.status(404).send({ message: _translations.entryLocal.translate('Entry is not found') });
      }
      done(null, entry);
    }).catch(function (err) {
      return res.send(err);
    });
  }, function (entry, done) {
    entry.remove().then(function () {
      res.status(200).send({ message: _translations.entryLocal.translate('Entry has been successfully removed') });
    }).catch(function (err) {
      return res.send(err);
    });
  }], function (err) {
    res.send(err);
  });
}

function _checkId(res, id, message) {
  if (!_mongodb.ObjectID.isValid(id)) {
    return res.status(404).send({ message: _translations.entryLocal.translate(message) });
  }
}

//-- Exports --//
exports.default = { add: add, getList: getList, getUsersList: getUsersList, getListWithCategory: getListWithCategory, get: get, update: update, remove: remove, getRecent: getRecent };