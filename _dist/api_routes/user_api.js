'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _async = require('async');

var _index = require('./../models/_index');

var _constants = require('./../configs/constants.config');

var _translations = require('./../assets/translations/translations');

var _mailGenerator = require('./../assets/helpers/mailGenerator');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * signUp user
 * @param  {[type]} req [request param]
 * @param  {[type]} res [reponse param]
 * @return {[type]}     [return user with auth token or error message]
 */
function signup(req, res, next) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.userLocal.setLocale(lang);

  var body = _underscore2.default.pick(req.body, ['email', 'password']);
  var user = new _index.User({ email: body.email, password: { hash: body.password } });

  (0, _async.waterfall)([function (done) {
    _randomBytes().then(function (token) {
      done(null, token);
    }).catch(function (err) {
      return done(err);
    });
  }, function (token, done) {
    _index.User.findOne({ email: body.email }, function (err, userExist) {
      if (err) {
        done(err);
      }

      if (userExist) {
        // done();
        return res.status(400).send({ message: _translations.userLocal.translate('User already exist. Please, use another email or just login if it\'s yours') });
        done();
      }

      var access = 'emailConfirm';
      var expiresIn = (0, _moment2.default)().add(1, 'days');
      user.tokens.push({ access: access, token: token, expiresIn: expiresIn });

      user.save(function (err) {
        done(err, token, user);
      });
    });
  }, function (token, user, done) {
    var profile = new _index.UserProfile({ _userId: user._id });
    profile.firstName = body.email.split('@')[0];
    profile.lastName = 'Doe';
    profile.gender = true;
    profile.birthdate = new Date();
    profile.avatar = null;
    profile.save(function (err) {
      done(err, token, user);
    });
  }, function (token, user, done) {
    var mailGenerator = new _mailGenerator.MailGenerator(user.email, process.env.EMAIL, _translations.userLocal.translate('Cubic House: Request to confirm your account'), '\n          <h2>' + _translations.userLocal.translate('Hello') + '</h2>\n          <h3>' + _translations.userLocal.translate('Thank you for registration') + '</h3>\n          <p>' + _translations.userLocal.translate('To complete the registration process, please activate the Cubic House Blog profile by running the following activation link') + '</p>\n          <p>\n            <span>Login: </span><span style="font-weight: bold">' + user.email + '</span>\n          </p>\n          <p style="width: 100%;\n                    padding: 30px;\n                    margin-bottom: 30px;\n                    background-color: #f0f5de;">\n            <a href="' + req.headers.origin + '/enter/email-confirm/' + token + '">' + _translations.userLocal.translate('Confirm email') + '</a>\n          </p>\n          <p style="font-weight: bold;">' + _translations.userLocal.translate('Warning!') + '</p>\n          <p>' + _translations.userLocal.translate('The activation link is valid for 24 hours. After this time to activate the profile, please send an email to info.cubichouse@gmail.com with the information in the subject line \"Activation Cubic House Blog\"') + '</p>\n        ');

    _mailGenerator.SMTP_TRANSPORT.sendMail(mailGenerator, function (err) {
      if (err) {

        _index.UserProfile.findOneAndRemove({ _userId: user._id }).then(function () {

          _index.User.findOneAndRemove({ _id: user._id }).then(function () {
            done(err);
          });
        });
      } else {

        done(null, user);
      }
    });
  }, function (user, done) {
    _index.User.findOne({ role: _constants.USER_ROLE.SUPER_ADMIN }, function (err, superAdmin) {

      var mailGenerator = new _mailGenerator.MailGenerator(superAdmin.email, process.env.EMAIL, _translations.userLocal.translate('Cubic House: New user account has been created'), '\n            <h2>' + _translations.userLocal.translate('Hello') + '</h2>\n            <h3>' + _translations.userLocal.translate('New user account has been created') + ':</h3>\n            <p>\n              <span>Id: </span><span style="font-weight: bold">' + user._id + '</span>\n            </p>\n            <p>\n              <span>Email: </span><span style="font-weight: bold">' + user.email + '</span>\n            </p>\n            <p>\n              <span>Created at: </span><span style="font-weight: bold">' + user.createdAt + '</span>\n            </p>\n          ');

      _mailGenerator.SMTP_TRANSPORT.sendMail(mailGenerator, function (err) {
        if (err) {
          done(err);
        }

        res.status(200).send({ message: _translations.userLocal.translate('An email has been sent to') + ' ' + user.email + ' ' + _translations.userLocal.translate('with further instructions') });
        done(null);
      });
    });
  }], function (err) {
    if (err) {
      return next(err);
    }
    res.send(err);
  });
}

/**
 * account confirm
 * @param  {[type]} req
 * @param  {[type]} res
 * @return {[type]}
 */


// app dependencies
//-- Dependencies --//
// lib dependencies
function accountConfirm(req, res) {
  var token = req.body.token;

  var lang = req.header(_constants.LANG_HEADER);
  _translations.userLocal.setLocale(lang);

  (0, _async.waterfall)([function (done) {
    _index.User.findOne({
      'tokens.access': 'emailConfirm',
      'tokens.token': token,
      'tokens.expiresIn': { $gt: (0, _moment2.default)().format() }
    }).then(function (user) {
      if (!user) {
        return res.status(400).send({ message: _translations.userLocal.translate('Confirm account token is invalid or has expired') });
      }

      user.removeToken(token).then(function () {
        done(null, user);
      }, function (err) {
        done(err);
      });
    });
  }, function (user, done) {
    var mailGenerator = new _mailGenerator.MailGenerator(user.email, process.env.EMAIL, _translations.userLocal.translate('Cubic House: Confirmation account'), '\n          <h2>' + _translations.userLocal.translate('Hello') + '</h2>\n          <p>' + _translations.userLocal.translate('Your email is successfully confirmed') + '</p>\n          <p>' + _translations.userLocal.translate('Now you can login to your blog account') + '</p>\n        ');

    _mailGenerator.SMTP_TRANSPORT.sendMail(mailGenerator, function (err) {
      if (err) {
        return next(err);
      }

      res.status(200).send({ message: _translations.userLocal.translate('Your email is successfully confirmed'), user: user });
      done(null);
    });
  }], function (err) {
    if (err) {
      return next(err);
    }
    res.send(err);
  });
}

/**
 * login user and generate auth token
 * @param  {[type]} req [request param]
 * @param  {[type]} res [rsponse param]
 * @return {[type]}     [if no error return user and auth header]
 */
function login(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.userLocal.setLocale(lang);

  var body = _underscore2.default.pick(req.body, ['email', 'password']);

  _index.User.findByCredentials(body.email, body.password, req).then(function (user) {

    return user.generateAuthToken(req).then(function (token) {
      _index.UserProfile.findOne({ _userId: user._id }).then(function (profile) {
        res.send({ user: user, profile: profile, token: token });
      });
    });
  }).catch(function (err) {
    res.status(400).send({ message: _translations.userLocal.translate(err) });
  });
}

/**
 * logout user and remove token
 * @param  {[type]} req
 * @param  {[type]} res
 * @return {[type]}
 */
function logout(req, res) {
  req.user.removeToken(req.token).then(function (user) {
    res.status(200).send(user);
  }, function () {
    res.status(400).send();
  });
}
/**
 * forgot password
 * generate random string for reseting password
 * @param {[type]}   req
 * @param {[type]}   res
 * @param {Function} next
 */
function forgotPassword(req, res, next) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.userLocal.setLocale(lang);

  (0, _async.waterfall)([function (done) {
    _randomBytes().then(function (token) {
      done(null, token);
    }).catch(function (err) {
      done(err);
    });
  }, function (token, done) {
    _index.User.findOne({ email: req.body.email }, function (err, user) {
      if (err) {
        done(err);
      }

      if (!user) {
        return res.status(400).send({ message: _translations.userLocal.translate('No account with that email address exists') });
      }

      var access = 'passwordReset';
      var expiresIn = (0, _moment2.default)().add(1, 'hours');
      user.tokens.push({ access: access, token: token, expiresIn: expiresIn });

      user.save(function (err) {
        done(err, token, user);
      });
    });
  }, function (token, user, done) {

    var mailGenerator = new _mailGenerator.MailGenerator(user.email, process.env.EMAIL, _translations.userLocal.translate('Cubic House: Password reset'), '\n          <h2>' + _translations.userLocal.translate('Hello') + '</h2>\n          <p>' + _translations.userLocal.translate('You are receiving this because you (or someone else) have requested the reset of the password for your account') + '</p>\n          <p>' + _translations.userLocal.translate('Please click on the following link, or paste this into your browser to complete the process:') + '</p>\n          <p>\n            <a href="' + req.headers.origin + '/enter/reset/' + token + '">' + _translations.userLocal.translate('Reset password') + '</a>\n          </p>\n          <p>' + _translations.userLocal.translate('If you did not request this, please ignore this email and your password will remain unchanged') + '</p>\n        ');

    _mailGenerator.SMTP_TRANSPORT.sendMail(mailGenerator, function (err) {
      if (err) {
        done(err);
      }

      res.status(200).send({ message: _translations.userLocal.translate('An email has been sent to') + ' ' + user.email + ' ' + _translations.userLocal.translate('with further instructions') });
      done(null);
    });
  }], function (err) {
    if (err) {
      return next(err);
    }
    res.send(err);
  });
}

function resetPassword(req, res, next) {
  var token = req.params.token;
  var lang = req.header(_constants.LANG_HEADER);
  _translations.userLocal.setLocale(lang);

  (0, _async.waterfall)([function (done) {
    _index.User.findOne({
      'tokens.access': 'passwordReset',
      'tokens.token': token,
      'tokens.expiresIn': { $gt: (0, _moment2.default)().format() }
    }).then(function (user) {
      if (!user) {
        return res.status(400).send({ message: _translations.userLocal.translate('Password reset token is invalid or has expired') });
      }

      user.removeToken(token).then(function () {
        user.password.hash = req.body.password;
        user.save(function (err) {
          done(err, user);
        });
      }, function () {
        done(err);
      });
    });
  }, function (user, done) {

    var mailGenerator = new _mailGenerator.MailGenerator(user.email, process.env.EMAIL, _translations.userLocal.translate('Cubic House: Password updated'), '\n          <h2>' + _translations.userLocal.translate('Hello') + '</h2>\n          <p>\n            ' + _translations.userLocal.translate('This is a confirmation that the password for your account') + '\n            ' + user.email + '\n            ' + _translations.userLocal.translate('has just been changed') + '\n          </p>\n        ');

    _mailGenerator.SMTP_TRANSPORT.sendMail(mailGenerator, function (err) {
      if (err) {
        done(err);
      }

      res.status(200).send({ message: _translations.userLocal.translate('Success! Your password has been changed') });
      done(null);
    });
  }], function (err) {
    if (err) {
      return next(err);
    }
    res.send(err);
  });
}

function updateEmail(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.userLocal.setLocale(lang);

  var email = _underscore2.default.pick(req.body, ['email']);
  _index.User.findOneAndUpdate({ _id: req.user._id }, { $set: email }, { new: true }).then(function (user) {
    if (!user) {
      return res.send({ message: _translations.userLocal.translate('User not found') });
    }

    res.status(200).send({ user: user, message: _translations.userLocal.translate('Email has been successfully updated') });
  });
}

function updatePassword(req, res) {
  var lang = req.header(_constants.LANG_HEADER);
  _translations.userLocal.setLocale(lang);
  var body = _underscore2.default.pick(req.body, ['password']);

  (0, _async.waterfall)([function (done) {
    _index.User.findOne({ _id: req.user._id }).then(function (user) {
      if (!user) {
        return res.status(400).send({ message: _translations.userLocal.translate('User not found') });
      }
      user.password.hash = body.password;
      user.generateAuthToken(req).then(function (token) {
        res.status(200).send({ user: user, token: token, message: _translations.userLocal.translate('Password has been successfully updated') });

        req.user.removeToken(req.token).then(function (user) {
          res.status(200).send(user);
        }, function () {
          res.status(400).send();
        });
      });
    });
  }, function (done) {}]);
}

function _randomBytes() {
  return new Promise(function (resolve, reject) {
    _crypto2.default.randomBytes(20, function (err, buf) {
      if (buf) {
        var token = buf.toString('hex');
        resolve(token);
      } else {
        reject(err);
      }
    });
  });
}

//-- Exports --//
exports.default = { signup: signup, accountConfirm: accountConfirm, login: login, logout: logout, forgotPassword: forgotPassword, resetPassword: resetPassword, updateEmail: updateEmail, updatePassword: updatePassword };