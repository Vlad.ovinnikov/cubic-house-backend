'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.routes = undefined;

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _constants = require('./../configs/constants.config');

var _authenticate = require('./../middleware/authenticate');

var _index = require('./_index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * routes
 * @param  {[type]} app app dependency
 */
function routes(app) {
  // API routes
  var router = _express2.default.Router();
  app.use('/api', router);

  app.options('*', (0, _cors2.default)());

  //-- User
  router.post('/signup', _index.userAPI.signup);
  router.post('/login', _index.userAPI.login);
  router.delete('/logout', (0, _authenticate.authenticateWithRoles)(''), _index.userAPI.logout);
  router.post('/forgot', _index.userAPI.forgotPassword);
  router.post('/reset/:token', _index.userAPI.resetPassword);
  router.post('/email-confirm', _index.userAPI.accountConfirm);

  //-- Profile
  router.get('/user/profile', (0, _authenticate.authenticateWithRoles)(''), _index.profileAPI.get);
  router.put('/user/profile', (0, _authenticate.authenticateWithRoles)(''), _index.profileAPI.update);
  router.put('/user/email', (0, _authenticate.authenticateWithRoles)(''), _index.userAPI.updateEmail);
  router.put('/user/password', (0, _authenticate.authenticateWithRoles)(''), _index.userAPI.updatePassword);

  //-- Category
  router.get('/categories', (0, _authenticate.authenticateWithRoles)(''), _index.categoryAPI.getList);
  router.get('/categories/:id', (0, _authenticate.authenticateWithRoles)(''), _index.categoryAPI.get);
  router.post('/categories', (0, _authenticate.authenticateWithRoles)([_constants.USER_ROLE.ADMIN, _constants.USER_ROLE.SUPER_ADMIN]), _index.categoryAPI.add);
  router.put('/categories/:id', (0, _authenticate.authenticateWithRoles)([_constants.USER_ROLE.ADMIN, _constants.USER_ROLE.SUPER_ADMIN]), _index.categoryAPI.update);
  router.delete('/categories/:id', (0, _authenticate.authenticateWithRoles)([_constants.USER_ROLE.ADMIN, _constants.USER_ROLE.SUPER_ADMIN]), _index.categoryAPI.remove);

  //-- Entry
  router.get('/categories/:id/entries', (0, _authenticate.authenticateWithRoles)(''), _index.entryAPI.getListWithCategory);
  router.get('/entries', (0, _authenticate.authenticateWithRoles)(''), _index.entryAPI.getList);
  router.get('/entries/recent', (0, _authenticate.authenticateWithRoles)(''), _index.entryAPI.getRecent);
  router.get('/user/entries', (0, _authenticate.authenticateWithRoles)(''), _index.entryAPI.getUsersList);
  router.get('/entries/:id', (0, _authenticate.authenticateWithRoles)(''), _index.entryAPI.get);
  router.post('/entries', (0, _authenticate.authenticateWithRoles)(''), _index.entryAPI.add);
  router.put('/entries/:id', (0, _authenticate.authenticateWithRoles)(''), _index.entryAPI.update);
  router.delete('/entries/:id', (0, _authenticate.authenticateWithRoles)(''), _index.entryAPI.remove);

  //-- Comment
  router.post('/comments', (0, _authenticate.authenticateWithRoles)(''), _index.commentAPI.add);
  router.get('/entries/:id/comments', (0, _authenticate.authenticateWithRoles)(''), _index.commentAPI.getListWithEntryId);
  router.get('/comments/recent', (0, _authenticate.authenticateWithRoles)(''), _index.commentAPI.getRecent);
  router.put('/comments/:id', (0, _authenticate.authenticateWithRoles)(''), _index.commentAPI.update);
  router.delete('/entries/:entryId/comments/:commentId', (0, _authenticate.authenticateWithRoles)(''), _index.commentAPI.remove);
}

//-- Exports --//


// app dependencies
//-- Dependencies --//
// lib dependencies
exports.routes = routes;