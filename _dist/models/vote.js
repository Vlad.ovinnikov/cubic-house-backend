'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// app dependencies

//-- Dependencies --//
// lib dependencies
var Schema = _mongoose2.default.Schema;


var VoteSchema = new Schema({
  createdAt: { type: String, default: (0, _moment2.default)().format() },
  updatedAt: { type: String, default: (0, _moment2.default)().format() },
  author: { type: Schema.Types.ObjectId, ref: 'User' }
});

// generate public JSON
VoteSchema.methods.toJSON = toJSON;

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  var vote = this;

  return {
    _id: vote._id,
    createdAt: vote.createdAt,
    updatedAt: vote.updatedAt,
    author: vote.author
  };
}

/**
 * Exports Comment model
 * @type {[type]}
 */
var Vote = _mongoose2.default.model('Vote', VoteSchema);
//-- Exports --//
exports.default = Vote;