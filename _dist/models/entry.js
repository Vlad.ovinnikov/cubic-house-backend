'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _constants = require('./../configs/constants.config');

var _index = require('./_index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// app dependencies
var Schema = _mongoose2.default.Schema; //-- Dependencies --//
// lib dependencies

var EntrySchema = new Schema({
  title: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    minlength: 3
  },
  description: {
    type: String,
    required: true,
    trim: true,
    minlength: 10
  },
  priority: {
    type: Number,
    default: _constants.PRIORITY.NONE
  },
  status: {
    type: Number,
    default: _constants.STATUS.DRAFT
  },
  createdAt: { type: String, default: (0, _moment2.default)().format() },
  updatedAt: { type: String, default: (0, _moment2.default)().format() },
  publishedAt: { type: String, default: (0, _moment2.default)().format() },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'UserProfile',
    required: true
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: 'Category',
    required: true
  },
  votes: [{
    type: Schema.Types.ObjectId,
    ref: 'Vote'
  }],
  comments: [{
    type: Schema.Types.ObjectId,
    ref: 'Comment'
  }]
});

// generate public JSON
EntrySchema.methods.toJSON = toJSON;
// update category date
EntrySchema.pre('save', updateDate);
EntrySchema.pre('remove', removeCascade);
/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  var entry = this;

  return {
    _id: entry._id,
    title: entry.title,
    description: entry.description,
    priority: entry.priority,
    status: entry.status,
    createdAt: entry.createdAt,
    updatedAt: entry.updatedAt,
    publishedAt: entry.publishedAt,
    author: entry.author,
    category: entry.category,
    comments: entry.comments
  };
}

/**
 * update date of category before save
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
function updateDate(next) {
  var entry = this;
  entry.updatedAt = (0, _moment2.default)().format();

  next();
}

function removeCascade(next) {
  _index.Comment.remove({ entry: this._id }).exec();

  next();
}

/**
 * Exports Entry model
 * @type {[type]}
 */
var Entry = _mongoose2.default.model('Entry', EntrySchema);
//-- Exports --//
exports.default = Entry;