'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

require('moment-timezone');

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _bcryptjs = require('bcryptjs');

var _bcryptjs2 = _interopRequireDefault(_bcryptjs);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _constants = require('./../configs/constants.config');

var _translations = require('./../assets/translations/translations');

var _mailGenerator = require('./../assets/helpers/mailGenerator');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//-- Implementation --//
//-- Dependencies --//
// lib dependencies
var Schema = _mongoose2.default.Schema;
// app dependencies

var UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    lowercase: true,
    required: 'Email is required',
    trim: true,
    minlength: 1,
    validate: {
      validator: _validator2.default.isEmail,
      message: 'Email is not valid'
    }
  },
  password: {
    hash: { type: String, required: true, trim: true, minlength: 6 },
    createdAt: { type: Date, default: (0, _moment2.default)().format() },
    updatedAt: { type: Date, default: (0, _moment2.default)().format() }
  },
  tokens: [{
    access: { type: String, require: true },
    token: { type: String, require: true },
    expiresIn: { type: Date }
  }],
  role: {
    type: String,
    enum: [_constants.USER_ROLE.USER, _constants.USER_ROLE.ADMIN, _constants.USER_ROLE.SUPER_ADMIN],
    default: _constants.USER_ROLE.USER
  },
  updatedAt: { type: Date, default: (0, _moment2.default)().format() },
  createdAt: { type: Date, default: (0, _moment2.default)().format() }
}, {
  timestamps: { createdAt: 'createdAt' }
});

// generate authentication token when user is creating
UserSchema.methods.generateAuthToken = generateAuthToken;
// find user by token
UserSchema.statics.findByToken = findByToken;
// find user by its credentials
UserSchema.statics.findByCredentials = findByCredentials;
// remove auth token
UserSchema.methods.removeToken = removeToken;
// generate public JSON
UserSchema.methods.toJSON = toJSON;
// hashing a password
UserSchema.pre('save', hashPassword);
UserSchema.pre('save', updateDate);

/**
 * implementation of generate authentication token when user is creating
 * @return {[type]} [return token to the user]
 */
function generateAuthToken(req) {
  var user = this;
  var access = 'auth';

  var expiresIn = (0, _moment2.default)().format();
  var token = _jsonwebtoken2.default.sign({
    _id: user._id.toHexString(),
    expiresIn: expiresIn
  }, process.env.TOKEN_SECRET).toString();

  // add token to the user
  user.tokens.push({ access: access, token: token, expiresIn: expiresIn });

  return user.save().then(function () {
    return token;
  });
}

/**
 * implementation find user by token
 * @param  {[type]} token [token]
 * @return {[type]}       [return user if token verified or reject promise]
 */
function findByToken(token) {
  var User = this;
  var decoded = void 0;

  try {
    decoded = _jsonwebtoken2.default.verify(token, process.env.TOKEN_SECRET);
  } catch (err) {
    return Promise.reject();
  }
  // TODO: add expiresIn for token
  // if(decoded) {
  //   return Promise.reject(`error1 ${moment().unix()}`);
  // }

  return User.findOne({
    _id: decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  }).then(function (user) {
    return user;
  });
}
/**
 * [find user by its credentials
 * @param  {[type]} email    [description]
 * @param  {[type]} password [description]
 * @return {[type]}          [description]
 */
function findByCredentials(email, password, req, next) {
  var User = this;
  return User.findOne({
    email: email
  }).then(function (user) {
    // if no user reject error
    if (!user) {
      return new Promise(function (resolve, reject) {
        reject('Authentication failed. User does not exist');
      });
    }

    return new Promise(function (resolve, reject) {
      _bcryptjs2.default.compare(password, user.password.hash, function (err, res) {
        // search token emailConfirm if found send error message
        // if not success
        _underscore2.default.find(user.tokens, function (token) {
          if (token.access === 'emailConfirm') {
            if ((0, _moment2.default)(token.expiresIn).isBefore((0, _moment2.default)().format())) {
              _randomBytes().then(function (newToken) {
                token.token = newToken;
                token.expiresIn = (0, _moment2.default)().add(1, 'days');

                user.save(function (err) {
                  if (err) {
                    reject(err);
                  }
                  next();
                });
              }).catch(function (err) {
                reject(err);
              });
            }
            var smtpTransport = _nodemailer2.default.createTransport({
              pool: true,
              host: 'smtp.gmail.com',
              port: 465,
              secure: true,
              auth: {
                user: process.env.EMAIL,
                pass: process.env.EMAIL_PASSWORD
              }
            });
            var mailGenerator = new _mailGenerator.MailGenerator(user.email, process.env.EMAIL, _translations.userLocal.translate('Cubic House: Request to confirm your account'), '\n                          <h2>' + _translations.userLocal.translate('Hello') + '</h2>\n                          <h3>' + _translations.userLocal.translate('Thank you for registration') + '</h3>\n                          <p>' + _translations.userLocal.translate('To complete the registration process, please activate the Cubic House Blog profile by running the following activation link') + '</p>\n                          <p>\n                            <span>Login: </span><span style="font-weight: bold">' + user.email + '</span>\n                          </p>\n                          <p style="width: 100%;\n                                    padding: 30px;\n                                    margin-bottom: 30px;\n                                    background-color: #f0f5de;">\n                            <a href="' + req.headers.origin + '/enter/email-confirm/' + token.token + '">' + _translations.userLocal.translate('Confirm email') + '</a>\n                          </p>\n                          <p style="font-weight: bold;">' + _translations.userLocal.translate('Warning!') + '</p>\n                          <p>' + _translations.userLocal.translate('The activation link is valid for 24 hours. After this time to activate the profile, please send an email to info.cubichouse@gmail.com with the information in the subject line \"Activation Cubic House Blog\"') + '</p>\n                        ');

            smtpTransport.sendMail(mailOptions, function (err) {
              if (err) {
                reject(err);
              }
              next();
            });
            reject('Authentication failed. You can not login before email is confirmed. We have been sent to you a new email with confirmation link');
          }
        });

        if (res) {
          resolve(user);
        } else {
          reject('Authentication failed. Password is not correct');
        }
      });
    });
  });
}

function removeToken(token) {
  var user = this;

  return user.update({
    $pull: {
      tokens: { token: token }
    }
  });
}

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  var user = this;

  return {
    _id: user._id,
    email: user.email,
    role: user.role,
    updatedAt: user.updatedAt,
    createdAt: user.createdAt,
    passwordUpdatedAt: user.password.updatedAt
  };
}
/**
 * update date
 * @param  {Function} next
 * @return {[type]}
 */
function updateDate(next) {
  var user = this;
  user.updatedAt = (0, _moment2.default)().format();
  next();
}

/**
 * generate hash for password recovering
 * @param  {Function} next
 * @return {[type]}
 */
function hashPassword(next) {
  var user = this;
  // user.updatedAt = moment().format();
  if (user.isModified('password') || user.isNew) {
    // user.createdAt = { type: String, default: moment().format() };
    // generate a salt
    _bcryptjs2.default.genSalt(_constants.SALT_WORK_FACTOR, function (err, salt) {
      if (err) next(err);
      // hash the password along with our new salt
      _bcryptjs2.default.hash(user.password.hash, salt, function (err, hash) {
        if (err) next(err);
        // override the cleartext password with the hashed one
        user.password.hash = hash;
        user.password.updatedAt = (0, _moment2.default)().format();
        next();
      });
    });
  } else {
    next();
  }
}

function _randomBytes() {
  return new Promise(function (resolve, reject) {
    _crypto2.default.randomBytes(20, function (err, buf) {
      if (buf) {
        var token = buf.toString('hex');
        resolve(token);
      } else {
        reject(err);
      }
    });
  });
}

/**
 * Exports User model
 * @type {[type]}
 */
var User = _mongoose2.default.model('User', UserSchema);

//-- Exports --//
exports.default = User;