'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _mongodb = require('mongodb');

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _index = require('./_index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// app dependencies
var Schema = _mongoose2.default.Schema; //-- Dependencies --//
// lib dependencies

var CategorySchema = new Schema({
  title: {
    type: String,
    required: true,
    unique: true,
    minlength: 3
  },
  description: {
    type: String,
    required: false
  },
  createdAt: { type: Date, default: (0, _moment2.default)().format() },
  updatedAt: { type: Date, default: (0, _moment2.default)().format() },
  publishedAt: { type: Date, default: (0, _moment2.default)().format() },
  author: { type: Schema.Types.ObjectId, ref: 'UserProfile' }
});

// generate public JSON
CategorySchema.methods.toJSON = toJSON;
// update category date
CategorySchema.pre('save', updateDate);
CategorySchema.pre('remove', removeCascade);

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  var category = this;

  return {
    _id: category._id,
    title: category.title,
    description: category.description,
    updatedAt: category.updatedAt,
    createdAt: category.createdAt,
    publishedAt: category.publishedAt,
    author: category.author
  };
}

/**
 * update date of category before save
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
function updateDate(next) {
  var category = this;
  category.title = category.title[0].toUpperCase() + category.title.slice(1);
  category.updatedAt = (0, _moment2.default)().format();

  next();
}

/**
 * remove cascade
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
function removeCascade(next) {

  _index.Entry.find({ category: this._id }).exec(function (err, entries) {
    _underscore2.default.each(entries, function (entry) {
      _index.Entry.remove({ _id: entry._id }).exec().then(function (res) {
        return _index.Comment.remove({ entry: entry._id }).exec();
      });
    });
  });

  next();
}

/**
 * Exports Category model
 * @type {[type]}
 */
var Category = _mongoose2.default.model('Category', CategorySchema);

//-- Exports --//
exports.default = Category;