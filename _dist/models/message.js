'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// app dependencies

//-- Dependencies --//
// lib dependencies
var Schema = _mongoose2.default.Schema;


var MessageSchema = new Schema({
  title: {
    type: String,
    required: true,
    minlength: 3
  },
  description: {
    type: String,
    required: true,
    minlength: 10
  },
  priority: {
    type: Number,
    required: true,
    default: 0
  },
  createdAt: { type: String, default: (0, _moment2.default)().format() },
  updatedAt: { type: String, default: (0, _moment2.default)().format() },
  sender: { type: Schema.Types.ObjectId, ref: 'User' },
  receiver: { type: Schema.Types.ObjectId, ref: 'Entry' }
});

// generate public JSON
MessageSchema.methods.toJSON = toJSON;

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  var message = this;

  return {
    _id: message._id,
    title: message.title,
    description: message.description,
    priority: message.priority,
    createdAt: message.createdAt,
    updatedAt: message.updatedAt,
    sender: message.sender,
    receiver: message.receiver
  };
}

/**
 * Exports Message model
 * @type {[type]}
 */
var Message = _mongoose2.default.model('Message', MessageSchema);
//-- Exports --//
exports.default = Message;