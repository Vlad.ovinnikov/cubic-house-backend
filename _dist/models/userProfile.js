'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// app dependencies

var Schema = _mongoose2.default.Schema; //-- Dependencies --//
// lib dependencies

var UserProfileSchema = new Schema({
  firstName: {
    type: String,
    required: false,
    minlength: 1
  },
  lastName: {
    type: String,
    required: false,
    minlength: 1
  },
  avatar: {
    type: Buffer,
    contentType: String,
    required: false,
    default: null
  },
  gender: {
    type: Boolean,
    required: false,
    default: true
  },
  birthdate: { type: Date, default: null },
  _userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: { createdAt: 'createdAt' }
});

// generate public JSON
UserProfileSchema.methods.toJSON = toJSON;

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  var profile = this;

  return {
    _id: profile._id,
    firstName: profile.firstName,
    lastName: profile.lastName,
    avatar: profile.avatar,
    gender: profile.gender,
    birthdate: profile.birthdate,
    updatedAt: profile.updatedAt,
    createdAt: profile.createdAt,
    _userId: profile._userId
  };
}

/**
 * Exports UserProfile model
 * @type {[type]}
 */
var UserProfile = _mongoose2.default.model('UserProfile', UserProfileSchema);
//-- Exports --//
exports.default = UserProfile;