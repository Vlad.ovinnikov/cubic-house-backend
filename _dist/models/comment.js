'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// app dependencies

//-- Dependencies --//
// lib dependencies
var Schema = _mongoose2.default.Schema;


var CommentSchema = new Schema({
  text: {
    type: String,
    required: true,
    trim: true,
    minlength: 3
  },
  parentId: {
    type: Schema.Types.ObjectId,
    ref: 'Comment',
    default: null
  },
  children: [{
    type: Schema.Types.ObjectId,
    ref: 'Comment'
  }],
  votes: [{
    type: Schema.Types.ObjectId,
    ref: 'UserProfile'
  }],
  createdAt: { type: String, default: (0, _moment2.default)().format() },
  updatedAt: { type: String, default: (0, _moment2.default)().format() },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'UserProfile'
  },
  entry: {
    type: Schema.Types.ObjectId,
    ref: 'Entry'
  }
});

// generate public JSON
CommentSchema.methods.toJSON = toJSON;
// update comment date
CommentSchema.pre('save', updateDate);
CommentSchema.pre('remove', removeCascade);

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  var comment = this;

  return {
    _id: comment._id,
    text: comment.text,
    votes: comment.votes,
    parentId: comment.parentId,
    createdAt: comment.createdAt,
    updatedAt: comment.updatedAt,
    author: comment.author,
    entry: comment.entry,
    children: comment.children
  };
}

/**
 * update date of comment before save
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
function updateDate(next) {
  var comment = this;
  comment.updatedAt = (0, _moment2.default)().format();

  next();
}

/**
 * remove nested comments
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
function removeCascade(next) {
  Comment.remove({ parentId: this._id }).exec();

  next();
}

/**
 * Exports Comment model
 * @type {[type]}
 */
var Comment = _mongoose2.default.model('Comment', CommentSchema);
//-- Exports --//
exports.default = Comment;