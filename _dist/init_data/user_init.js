'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _index = require('./../models/_index');

var _constants = require('./../configs/constants.config');

function createSuperUser() {
  var superAdmin = new _index.User({
    email: 'vlad.ovinnikov@gmail.com',
    password: { hash: 'Vlad12345' },
    role: _constants.USER_ROLE.SUPER_ADMIN
  });
  _index.User.findOne({ email: superAdmin.email }).then(function (superAdminExists) {
    if (!superAdminExists) {

      superAdmin.save(function (err) {
        if (err) {
          console.log(err);
        } else {
          console.log('SuperAdmin has successfully created!');
        }
      });

      var profile = new _index.UserProfile({ _userId: superAdmin._id });
      profile.firstName = superAdmin.email.split('@')[0];
      profile.lastName = 'Doe';
      profile.save(function (err) {
        if (err) {
          console.log(err);
        } else {
          console.log('SuperAdmins profile has successfully created!');
        }
      });
    } else {
      console.log('SuperAdmin with that email has already existed!');
    }
  }).catch(function (err) {
    return console.log(err);
  });
}

exports.default = { createSuperUser: createSuperUser };