'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _db = require('./../configs/db.config');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//-- Implementation --//
//-- Dependencies --//
// lib dependencies
_mongoose2.default.Promise = global.Promise;
// app dependencies


function connectToDatabase() {
  var dbConnection = void 0;

  if (process.env.NODE_ENV === 'test') {
    dbConnection = process.env.MONGODB_URI;
  } else if (process.env.NODE_ENV === 'development') {
    dbConnection = 'mongodb://' + process.env.MONGODB_USER + ':' + process.env.MONGODB_PASSWORD + process.env.MONGODB_URI;
  } else {
    dbConnection = 'mongodb://' + process.env.MONGODB_USER + ':' + process.env.MONGODB_PASSWORD + process.env.MONGODB_URI;
  }

  if (process.env.NODE_ENV !== 'test') {
    console.log('User     = ' + process.env.MONGODB_USER);
    console.log('Password = ' + process.env.MONGODB_PASSWORD);
    console.log('EMAIL    = ' + process.env.EMAIL);
    console.log('EMAIL_PASSWORD = ' + process.env.EMAIL_PASSWORD);
  }

  console.log('EMAIL    = ' + process.env.EMAIL);
  console.log('EMAIL_PASSWORD = ' + process.env.EMAIL_PASSWORD);
  console.log('NODE_ENV    = ' + process.env.NODE_ENV);
  console.log('MONGODB_URI = ' + process.env.MONGODB_URI);

  return _mongoose2.default.connect(dbConnection, function (err) {
    if (!err) {
      console.log('Connected to MongoDb server: ' + dbConnection);
    } else {
      console.error('Unable to connect to MongoDb server.\n            Error: ' + err);
    }
  }).connection;
}
connectToDatabase().on('error', console.error.bind(console)).on('disconnected', connectToDatabase);

//-- Exports --//
exports.default = { mongoose: _mongoose2.default };