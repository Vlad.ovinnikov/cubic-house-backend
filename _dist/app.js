'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _cookieParser = require('cookie-parser');

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _mongoose = require('./db/mongoose');

var _routes = require('./api_routes/routes');

var _user_init = require('./init_data/user_init');

var _user_init2 = _interopRequireDefault(_user_init);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//-- Implementation --//
//-- Dependencies --//
// lib dependencies
var app = (0, _express2.default)();
// app dependencies


app.use((0, _morgan2.default)('dev'));
app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: false }));
app.use((0, _cookieParser2.default)());

var originsWhitelist = ['http://localhost:4200', //this is my front-end url for development
'https://cubic-house-front.herokuapp.com'];

var corsOptions = {
  origin: function origin(_origin, callback) {
    callback(null, originsWhitelist.indexOf(_origin) !== -1);
  },
  credentials: true
};

app.use((0, _cors2.default)(corsOptions));

// Main app routes
(0, _routes.routes)(app);

_user_init2.default.createSuperUser();

/**
 * Catch 404 and forward to error handler
 * @type {Error}
 */
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/**
 * Error handler
 * @type {[type]}
 */
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: req.app.get('env') === 'development' ? err : {}
  });
});

//-- Exports --//
exports.default = app;