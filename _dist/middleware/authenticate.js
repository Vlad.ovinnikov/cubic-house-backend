'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.authenticateWithRoles = undefined;

var _user = require('./../models/user');

var _user2 = _interopRequireDefault(_user);

var _constants = require('./../configs/constants.config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * check if user authenticated and if authorized
 * @param  {[type]} roles [user roles]
 * @return {[type]}       [return error or 200]
 */
//-- Dependencies --//
// app dependencies
var authenticateWithRoles = function authenticateWithRoles(roles) {

  return function (req, res, next) {
    var token = req.header(_constants.AUTH_HEADER);

    _user2.default.findByToken(token).then(function (user) {
      if (!user) {
        return Promise.reject({ error: 'No user' });
      }

      req.user = user;
      req.token = token;

      if (roles) {
        if (roles.indexOf(user.role) > -1) {
          next();
        } else {
          return Promise.reject({ error: 'You are not authorized to view this content' });
        }
      } else {
        next();
      }
    }).catch(function (err) {
      res.status(401).send(err);
    });
  };
};

//-- Exports --//
exports.authenticateWithRoles = authenticateWithRoles;