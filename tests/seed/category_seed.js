//-- Dependencies --//
// lib dependencies
import { ObjectID } from 'mongodb';
// app dependencies
import Category from './../../models/category';
import User from './../../models/user';

const userOneId = new ObjectID(),
      userTwoId = new ObjectID(),
      categoryOneId = new ObjectID(),
      categoryTwoId = new ObjectID();

const categories = [
  {
    _id: categoryOneId,
    title: 'Category #1',
    description: 'Description of category #1',
    author: userOneId
  },
  {
    _id: categoryTwoId,
    title: 'Category #2',
    description: 'Description of category #2',
    author: userTwoId
  }
];

const populatesCategories = (done) => {
  Category.remove({})
    .then(() => {
      let categoryOne = new Category(categories[0]).save(),
          categoryTwo = new Category(categories[1]).save();

          return Promise.all([categoryOne, categoryTwo]);
        })
        .then(() => done());
}

export { categories, populatesCategories };
