//-- Dependencies --//
// lib dependencies
import { ObjectID } from 'mongodb';
// app dependencies
import Entry from './../../models/entry';
import { users } from './user_seed';
import { categories } from './category_seed';
import { PRIORITY, STATUS } from './../../configs/constants.config';

const entryOneId = new ObjectID(),
      entryTwoId = new ObjectID(),
      entryThreeId = new ObjectID();

const entries = [
  {
    _id: entryOneId,
    title: 'Entry #1',
    description: 'Description of entry #1',
    priority: PRIORITY.NONE,
    status: STATUS.DRUFT,
    category: categories[0]._id,
    author: users[0]._id
  },
  {
    _id: entryTwoId,
    title: 'Entry #2',
    description: 'Description of entry #2',
    priority: PRIORITY.LOW,
    status: STATUS.DRUFT,
    category: categories[1]._id,
    author: users[0]._id
  },
  {
    _id: entryThreeId,
    title: 'Entry #3',
    description: 'Description of entry #3',
    priority: PRIORITY.HIGH,
    status: STATUS.ARCHIVED,
    category: categories[0]._id,
    author: users[1]._id
  }
];

const populatesEntries = (done) => {
  Entry.remove({})
    .then(() => {
      let entryOne = new Entry(entries[0]).save(),
          entryTwo = new Entry(entries[1]).save(),
          entryThree = new Entry(entries[2]).save();

          return Promise.all([entryOne, entryTwo, entryThree]);
        })
        .then(() => done());
}

export { entries, populatesEntries };
