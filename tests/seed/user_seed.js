//-- Dependencies --//
// lib dependencies
import { ObjectID } from 'mongodb';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import moment from 'moment';
// app dependencies
import User from './../../models/user';
import { USER_ROLE } from './../../configs/constants.config';

const userOneId = new ObjectID(),
      userTwoId = new ObjectID(),
      userThreeId = new ObjectID();

const users = [
  {
    _id: userOneId,
    email: 'vlad@gmail.com',
    password: {hash: 'userOnePass'},
    role: USER_ROLE.ADMIN,
    tokens: [
      {
        access: 'auth',
        token: jwt.sign({_id: userOneId, access: 'auth'}, process.env.TOKEN_SECRET).toString()
      },
      {
        access: 'emailConfirm',
        token: '12344rtv4tdcv',
        expiresIn: moment()
      }
    ]
  },
  {
    _id: userTwoId,
    email: 'vlad2@gmail.com',
    password: {hash: 'userTwoPass'},
    role: USER_ROLE.USER
  },
  {
    _id: userThreeId,
    email: 'vlad1@gmail.com',
    password: {hash: 'userThreePass'},
    role: USER_ROLE.USER,
    tokens: [
      {
        access: 'auth',
        token: jwt.sign({_id: userThreeId, access: 'auth'}, process.env.TOKEN_SECRET).toString()
      }
    ]
  },
];

const populatesUsers = (done) => {
  User.remove({})
    .then(() => {
      let userOne = new User(users[0]).save(),
          userTwo = new User(users[1]).save();

          return Promise.all([userOne, userTwo]);
        })
        .then(() => done());
}

export { users, populatesUsers };
