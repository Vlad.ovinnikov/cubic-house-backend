//-- Dependencies --//
//-- lib dependencies
import app from './../../app';
import expect from 'expect';
import request from 'supertest';
import { ObjectID } from 'mongodb';
// app dependencies
import { populatesEntries, entries } from './../seed/entry_seed';
import { categories } from './../seed/category_seed';
import { users } from './../seed/user_seed';
import Entry from './../../models/entry';
import { AUTH_HEADER } from './../../configs/constants.config';

beforeEach(populatesEntries);

describe('<-- Get entries -->', () => {
  it('should return list of entries', (done) => {
    request(app)
      .get(`/api/categories/${entries[1].category.toHexString()}/entries`)
      .expect(200)
      .expect((res) => {
        expect(res.body.length).toBe(1);
      })
      .end(done);
  });

  it('should return 404 if category not found', (done) => {
    request(app)
      .get(`/api/categories/${new ObjectID().toHexString()}/entries`)
      .expect(404)
      .end(done);
  });

  it('should return 404 if non-object ids', (done) => {
    request(app)
      .get(`/api/categories/abc123/entries`)
      .expect(404)
      .end(done);
  });
});

describe('<-- Get entry -->', () => {
  it('should return entry', (done) => {
    request(app)
      .get(`/api/entries/${entries[0]._id}`)
      .expect(200)
      .expect((res) => {
        expect(res.body.title).toBe(entries[0].title);
        expect(res.body.description).toBe(entries[0].description);
        expect(res.body.priority).toBe(entries[0].priority);
        expect(res.body.status).toBe(entries[0].status);
        expect(res.body.category).toBe(entries[0].category.toHexString());
        expect(res.body.author).toBe(entries[0].author.toHexString());
      })
      .end(done);
  });

  it('should return 404 if entry not found', (done) => {
    request(app)
      .get(`/api/entries/${new ObjectID().toHexString()}`)
      .expect(404)
      .end(done);
  });

  it('should return 404 if non-object ids', (done) => {
    request(app)
      .get(`/api/entries/abc123`)
      .expect(404)
      .end(done);
  });
});

describe('<-- Add new entry -->', () => {
  it('should add new entry', (done) => {
    let entry = {title: 'New entry', description: 'New entry description', priority: 1, status: 2, category: categories[0]._id, author: users[0]._id};

    request(app)
      .post('/api/entries')
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send(entry)
      .expect((res) => {
        expect(res.body.title).toBe(entry.title);
        expect(res.body.description).toBe(entry.description);
        expect(res.body.priority).toBe(entry.priority);
        expect(res.body.status).toBe(entry.status);
        expect(res.body.category).toBe(entry.category.toHexString());
        expect(res.body.author).toBe(entry.author.toHexString());
      })
      .end((err, res) => {
        if(err) {return done(err);}
        Entry.find({title: entry.title})
          .then((entries) => {
            expect(entries.length).toBe(1);
            expect(entries[0].title).toBe(entry.title);
            expect(entries[0].description).toBe(entry.description);
            expect(entries[0].priority).toBe(entry.priority);
            expect(entries[0].status).toBe(entry.status);
            expect(entries[0].category.toHexString()).toBe(entry.category.toHexString());
            expect(entries[0].author.toHexString()).toBe(entry.author.toHexString());
            done();
          })
          .catch((err) => done(err));
      });
  });

  it('should return 400 when try to add entry with same title', (done) => {
    request(app)
      .post('/api/entries')
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send(entries[0])
      .expect(400)
      .end(done);
  });

  it('should return 401 when user not authenticated', (done) => {
    let entry = {title: 'New entry', description: 'New entry description', priority: 1, status: 2, category: categories[0]._id, author: users[0]._id};

    request(app)
      .post('/api/entries')
      .send(entry)
      .expect(401)
      .end(done);
  });

  it('should return 400 if not valid body', (done) => {
    request(app)
      .post('/api/entries')
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({})
      .expect(400)
      .end(done);
  });

  it('should return 400 if description has minlength error', (done) => {
    let entry = {title: 'New entry', description: 'New', priority: 1, status: 2, category: categories[0]._id, author: users[0]._id};

    request(app)
      .post('/api/entries')
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({entry})
      .expect(400)
      .end(done);
  });

  it('should return 400 if title has minlength error', (done) => {
    let entry = {title: 'Ne', description: 'New entry description', priority: 1, status: 2, category: categories[0]._id, author: users[0]._id};

    request(app)
      .post('/api/entries')
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({entry})
      .expect(400)
      .end(done);
  });
});

describe('<-- Update entry -->', () => {
  it('should update entry', (done) => {
    let title = 'Updated entry title',
        description = 'Updated entry description';

    request(app)
      .put(`/api/entries/${entries[0]._id}`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({title, description})
      .expect(200)
      .expect((res) => {
        expect(res.body.title).toBe(title);
        expect(res.body.description).toBe(description);
      })
      .end((err, res) => {
        if(err) {return done(err);}

        Entry.findById(entries[0]._id)
          .then((res) => {
            expect(res.title).toBe(title);
            expect(res.description).toBe(description);

            done();
          })
          .catch((err) => done(err));
      })
  });

  it('should return 404 if non-object ids', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .put(`/api/entries/123abc`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({title, description})
      .expect(404)
      .end(done);
  });

  it('should return 404 if entry not found', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .put(`/api/entries/${new ObjectID}`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({title, description})
      .expect(404)
      .end(done);
  });

  it('should return 401 if user not authorized', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .put(`/api/entries/${entries[0]._id}`)
      .send({title, description})
      .expect(401)
      .end(done);
  });

  it('should return 404 if user not owner', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .put(`/api/entries/${entries[0]._id}`)
      .set(AUTH_HEADER, users[2].tokens[0].token)
      .send({title, description})
      .expect(401)
      .end(done);
  });
});

describe('<-- Remove entry -->', () => {
  it('should remove entry', (done) => {
    request(app)
      .delete(`/api/entries/${entries[0]._id}`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body._id).toBe(entries[0]._id);
      })
      .end((err, res) => {
        Entry.find({})
          .then((entries) => {
            expect(entries.length).toBe(2);
            done();
          })
          .catch((err) => done(err));
      });
  });

  it('should return 404 if non-object ids', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .delete(`/api/entries/123abc`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({title, description})
      .expect(404)
      .end(done);
  });

  it('should return 404 if entry not found', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .delete(`/api/entries/${new ObjectID}`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({title, description})
      .expect(404)
      .end(done);
  });

  it('should return 401 if user not authorized', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .delete(`/api/entries/${entries[0]._id}`)
      .send({title, description})
      .expect(401)
      .end(done);
  });

  it('should return 404 if user not owner', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .delete(`/api/entries/${entries[0]._id}`)
      .set(AUTH_HEADER, users[2].tokens[0].token)
      .send({title, description})
      .expect(401)
      .end(done);
  });
});
