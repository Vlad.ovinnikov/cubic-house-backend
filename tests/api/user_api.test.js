//-- Dependencies --//
//-- lib dependencies
import app from './../../app';
import expect from 'expect';
import request from 'supertest';
// app dependencies
import { populatesUsers, users } from './../seed/user_seed';
import User from './../../models/user';
import UserProfile from './../../models/userProfile';
import { AUTH_HEADER, LANG_HEADER } from './../../configs/constants.config';

beforeEach(populatesUsers);

describe('<-- Signup user -->', () => {
  it('should create new user', (done) => {
    let email = 'vlad.test@gmail.com',
        password = 'Test12345';

    request(app)
      .post('/api/signup')
      .set(LANG_HEADER, 'pl')
      .send({ email, password })
      .expect(200)
      .expect((res) => {
        expect(res.body.message).toExist();
      })
      .end((err) => {
        if(err) {return done(err);}

        User.findOne({ email })
          .then((user) => {
            expect(user).toExist();
            expect(user.password.hash).toNotBe(password);
            // done();
            UserProfile.findOne({ _userId: user._id })
              .then((profile) => {
                expect(profile).toExist();
                done();
            })
            .catch((err) => done(err));
        })
        .catch((err) => done(err));
      });
  });

  it('should confirm account', (done) => {
    const token = '12344rtv4tdcv';

    request(app)
      .post('.api/email-confirm')
      .set(LANG_HEADER, 'pl')
      .send({ token })
      .expect(200)
      .expect((res) => {
        expect(res.body.message).toExist();
        expect(res.body.user).toExist();
      })
      .end(done)
  });

  // it('should return validation errors if request invalid', (done) => {
  //   request(app)
  //     .post('/api/signup')
  //     .set(LANG_HEADER, 'pl')
  //     .send({ email: 'hello', password: '123' })
  //     .expect(400)
  //     .end(done);
  // });

  it('should not create user if email in use', (done) => {
    request(app)
      .post('/api/signup')
      .set(LANG_HEADER, 'pl')
      .send({ email: users[0].email, password: '123Hello!' })
      .expect(400)
      .end(done);
  });
});

describe('<-- Get user profile -->', () => {
  it('should return user if authenticated', (done) => {
    request(app)
      .get('/api/users/profile')
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.user._id).toBe(users[0]._id.toHexString());
        expect(res.body.user.email).toBe(users[0].email);
      })
      .end(done);
  });

  it('should return 401 if not authenticated', (done) => {
    request(app)
      .get('/api/users/profile')
      .expect(401)
      .expect((res) => {
        expect(res.body).toEqual({});
      })
      .end(done);
  });
});

describe('<-- Login user -->', () => {
  it('should login user and return user, profile and token', (done) => {
    request(app)
      .post('/api/login')
      .set(LANG_HEADER, 'pl')
      .send({
        email: users[1].email,
        password: users[1].password.hash
      })
      .expect(200)
      .expect((res) => {
        expect(res.body.user).toExist();
        // expect(res.body.profile).toExist();
        expect(res.body.token).toExist();
      })
      .end((err, res) => {
        if(err) {
          return done(err);
        }

        User.findById(users[1]._id)
          .then((user) => {
            expect(user.tokens[0].access).toBe('auth');
            done();
          })
          .catch((err) => done(err));
      });
  });

  it('should reject invalid login', (done) => {
    request(app)
      .post('/api/login')
      .set(LANG_HEADER, 'pl')
      .send({
        email: users[1].email,
        password: users[1].password.hash + '1'
      })
      .expect(400)
      .expect((res) => {
        expect(res.headers[AUTH_HEADER]).toNotExist();
      })
      .end((err, res) => {
        if(err) {
          return done(err);
        }

        User.findById(users[1]._id)
          .then((user) => {
            expect(user.tokens.length).toBe(0);
            done();
          })
          .catch((err) => done(err));
      })
  });
});

describe('Delete auth token', () => {
  it('should remove auth token on logout', (done) => {
      request(app)
        .delete('/api/users/profile/token')
        .set(AUTH_HEADER, users[0].tokens[0].token)
        .expect(200)
        .end((err, res) => {
          if(err) {
            return done(err);
          }

          User.findById(users[0]._id)
            .then((user) => {
              expect(user.tokens.length).toBe(1);
              done();
            })
            .catch((err) => done(err));
        })
  });
});
