//-- Dependencies --//
//-- lib dependencies
import app from './../../app';
import expect from 'expect';
import request from 'supertest';
import { ObjectID } from 'mongodb';
// app dependencies
import { populatesCategories, categories } from './../seed/category_seed';
import Category from './../../models/category';
import { users } from './../seed/user_seed';
import { AUTH_HEADER } from './../../configs/constants.config';

beforeEach(populatesCategories);

describe('<-- Get categories -->', () => {
  it('should return list of categories', (done) => {
    request(app)
      .get('/api/categories')
      .expect(200)
      .expect((res) => {
        expect(res.body.length).toBe(2);
      })
      .end(done);
  });
});

describe('<-- Get category -->', () => {
  it('should return category', (done) => {
    request(app)
      .get(`/api/categories/${categories[0]._id.toHexString()}`)
      .expect(200)
      .expect((res) => {
        expect(res.body.title).toBe(categories[0].title);
        expect(res.body.description).toBe(categories[0].description);
        expect(res.body._userId).toBe(categories[0]._userId);
      })
      .end(done);
  });

  it('should return 404 if category not found', (done) => {
    let hexId = new ObjectID().toHexString();

    request(app)
      .get(`/api/categories/${hexId}`)
      .expect(404)
      .end(done);
  });

  it('should return 404 if non-object ids', (done) => {
    request(app)
      .get(`/api/categories/123abc`)
      .expect(404)
      .end(done);
  });
});

describe('<-- Add new category -->', () => {
  it('should add new category', (done) => {
    let newCategory = {title: 'New category', description: 'Description for new category', author: users[0]._id};

    request(app)
      .post('/api/categories')
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send(newCategory)
      .expect(200)
      .expect((res) => {
        expect(res.body.title).toBe(newCategory.title);
        expect(res.body.description).toBe(newCategory.description);
      })
      .end((err, res) => {
        if(err) {return done(err);}

        Category.find({title: newCategory.title})
          .then((categories) => {
            expect(categories.length).toBe(1);
            expect(categories[0].title).toBe(newCategory.title);
            expect(categories[0].description).toBe(newCategory.description);

            done();
          })
          .catch((err) => done(err));
        });
  });

  it('should return 400 when try to add category with same title', (done) => {
    request(app)
      .post('/api/categories')
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send(categories[0])
      .expect(400)
      .end(done);
  });

  it('should return 401 when user not authorized to do it', (done) => {
    let newCategory = {title: 'New category', description: 'Description for new category', author: users[0]._id};

    request(app)
      .post('/api/categories')
      .send(newCategory)
      .expect(401)
      .end(done);
  });

  it('should return 400 with not valid body', (done) => {
    request(app)
      .post('/api/categories')
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({})
      .expect(400)
      .end(done);
  });
});

describe('<-- Update category -->', () => {
  it('should update category', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .put(`/api/categories/${categories[0]._id}`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({title, description})
      .expect(200)
      .expect((res) => {
        expect(res.body.title).toBe(title);
        expect(res.body.description).toBe(description);
      })
      .end((err, res) => {
        if(err) {return done(err);}

        Category.findById(categories[0]._id)
          .then((res) => {
            expect(res.title).toBe(title);
            expect(res.description).toBe(description);

            done();
          })
          .catch((err) => done(err));
      })
  });

  it('should return 404 if non-object ids', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .put(`/api/categories/123abc`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({title, description})
      .expect(404)
      .end(done);
  });

  it('should return 404 if category not found', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .put(`/api/categories/${new ObjectID}`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({title, description})
      .expect(404)
      .end(done);
  });

  it('should return 401 if user not authorized', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .put(`/api/categories/${categories[0]._id}`)
      .send({title, description})
      .expect(401)
      .end(done);
  });
});

describe('<-- Remove category -->', () => {
  it('should remove category', (done) => {
    request(app)
      .delete(`/api/categories/${categories[0]._id}`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body._id).toBe(categories[1]._id);
      })
      .end((err, res) => {
        Category.find({})
          .then((categories) => {
            expect(categories.length).toBe(1);
            done();
          })
          .catch((err) => done(err));
      });
  });

  it('should return 404 if non-object ids', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .delete(`/api/categories/123abc`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({title, description})
      .expect(404)
      .end(done);
  });

  it('should return 404 if category not found', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .delete(`/api/categories/${new ObjectID}`)
      .set(AUTH_HEADER, users[0].tokens[0].token)
      .send({title, description})
      .expect(404)
      .end(done);
  });

  it('should return 401 if user not authorized', (done) => {
    let title = 'Updated title',
        description = 'Updated description';

    request(app)
      .delete(`/api/categories/${categories[0]._id}`)
      .send({title, description})
      .expect(401)
      .end(done);
  });
});
