//-- Dependencies --//
// app dependencies
import User from './../models/user';
import { AUTH_HEADER } from './../configs/constants.config';

/**
 * check if user authenticated and if authorized
 * @param  {[type]} roles [user roles]
 * @return {[type]}       [return error or 200]
 */
let authenticateWithRoles = (roles) => {

  return (req, res, next) => {
    let token = req.header(AUTH_HEADER);

    User.findByToken(token)
      .then((user) => {
        if(!user) { return Promise.reject({error: 'No user'});}

        req.user = user;
        req.token = token;

        if(roles){
          if(roles.indexOf(user.role) > -1) {
            next();
          } else {
            return Promise.reject({error: 'You are not authorized to view this content'});
          }
        } else {
          next();
        }

      })
      .catch((err) => {
        res.status(401).send(err);
      });
  }
}

//-- Exports --//
export { authenticateWithRoles };
