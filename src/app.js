//-- Dependencies --//
// lib dependencies
import express from 'express';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors';
// app dependencies
import { mongoose } from './db/mongoose';
import { routes } from './api_routes/routes';
import userInit from './init_data/user_init';

//-- Implementation --//
let app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

const originsWhitelist = [
  'http://localhost:4200',      //this is my front-end url for development
  'https://cubic-house-front.herokuapp.com'
];

let corsOptions = {
  origin: function(origin, callback) {
    callback(null, originsWhitelist.indexOf(origin) !== -1);
  },
  credentials: true
}

app.use(cors(corsOptions));

// Main app routes
routes(app);

userInit.createSuperUser();

/**
 * Catch 404 and forward to error handler
 * @type {Error}
 */
app.use((req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/**
 * Error handler
 * @type {[type]}
 */
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: req.app.get('env') === 'development' ? err : {}
  });
});

//-- Exports --//
export default app;
