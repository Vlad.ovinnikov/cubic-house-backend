import nodemailer from 'nodemailer';
import smtpTransport from 'nodemailer-smtp-transport';

export const SMTP_TRANSPORT = nodemailer.createTransport(smtpTransport({
  pool: true,
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  debug: true,
  auth: {
    user: process.env.EMAIL,
    pass: process.env.EMAIL_PASSWORD
  }
}));

export class MailGenerator {
  constructor(to, from, subject, html) {
    this.to = to;
    this.from = from;
    this.subject = subject;
    this.html = `
      <header style="position: absolute;
                    top: 0;
                    left: 0;
                    right: 0;
                    background-color: #f0f5de;
                    border-bottom: 3px solid #41ad48;
                    padding: 10px;">
        <img style="display: inline-block;
                    width: 70px;
                    float: left;" src="data:image/false;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAYAAABxLuKEAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4QMIEzcqxgREcwAAETpJREFUeNrtm3mQVdWdxz/n3Hvf0iv0As0iIDSLgois42gZoqW4TtRMmxhNQlxIUCPIaFwSzUtSGh0dY1wgwdHMmOiUUloxcSkzElvchoiAEBYDQQaQrRsaul/32+49Z/64972+3by1u7WcKk/Vq9f93ll+53t+v+9vOffBFy1rMwZ8Ro1AIGlGf0Z7EN7rc9wiyLz/fxqH8Fmt1Q9ABMCMBVgsZGy27z4VUK6ikibCGe0ZIICMfqtxBEkEBcDVNO4bEVjE9KrnmWhHmax28QDtGYCaB9SAXHOdE5jACcbPmeh8xPu00IweiLWMfvNIM5pvUcs51hXMrPkvysU4DidWM6LqMmrFRUxUhxmpd7GUhA+g/vPPXATNaE5xBjHV+Xcaai5lYiLBeL2Lh+jo72EYfTKbL3uAnEeQC41zmBT8NxprF9Pa/hYppw1ThOlK7kaRoNG6lUpjHOPt/YzhE5aiBoSg08DMDNTTUHUxh7vWMmbQt6njHE4QRxjp9OswjJKUF08YgIXMYLL1IybbDxKz9tAR24whggjhzimFCUCH2kLYGM6E1J0YgTImOp9w0QCofBqYU406RlctJp7cS1diN1orGhO3UhkazyT2M1q5h1Gi95JF8whoQLMwOJY7A3dxGmuoDc7hQPBlNClMEc4yUmCJMlK6gwOhVxgRupgzwpu43bqRBWXDPG5yAdL9IWjtHpYhQiDgQOhlyuRo5tirODFwNwuDEzPyF+kMRBHfpzVkMBXWVzhp0EMoneJw9C8ITAwRQBehpgKBoxNoFA3V53Ekuo5t8Z/R6bzGcroAIhFkJE3kxZh0BMUNgUnMqVlDa/s7CGUihPDWSqFJUhk8kfKy4/mw9Tr2pp7ntxw8Zm8lmpI7sKkpwJmbv0Rj4H4ajVtotVcRS+zBFGUIUZprFMJACpP2+BY0No3J2xCB4Zwo9zFatTQvxQZEJIJsLsQJaVOabdQxMryArsRuhJYIIby1JEIESKqDRGPbaaxdRI06nQmqhVF6F5tJ5VMMmQ+UKyJUHT9jxd3MZiVWeS37Un9AKBNThIvSkpzHIcpAOOwLP89xg7/GtNTqwGguuOZhRgI6EkFFSop/tM6xC43UAQwRZF/bKzg6xmznRaaHfs13qPc0RhQNTCTidjaqqLzzVG6+El4ut9vaqDxuDNKQOLbdj8BMo3QXBpUMi32VHYeXsSY46awEr4xs4J7Fv+H737qHWs+kdCSC1AX5RxQG0PSCwP3BFzku/k1C1HgmWRIwGkAJVHsn62bXMviOsVRdaO1sBSA4dDBaKbRSJYHi6ARaK4ba5xN39rHanMsbqdtYlvhoTi1OyuHo5NE8PGUKz3/3Uc6f/2NCkQhKCBegfqdUAoEUYVKD9+Fg98srCUEw5qDCBvKcBsp+OKLTnGXsacOsrsCsKEPZDlrrvKSrhI2j4wwqP4WwNZy18husT8znEftNml1e+eNehAbREWedlARmTebl0ZNZdu1DzPJ4p0TzyrNlM1iNk38es6ipJMLRaMeBISHMK0dScWr0YOL3B4nvCo2qJ9nWgYolkJbZExAcHB0nJEcwqHoaGw/dwlHrWZYmd/s8iwb0jBkgBCJoMrgzzs7Wdl6vH8QZx9Uzf9BT/LS1lf+MLGFHelzTJsSKQhzTj2YW71Bcqkoq18waKwncWIa1/siuo8+1kkiGxzQQ338IbSuEJXF0F4aoZEj8LHZYj7Kx7dssszeA3Q1IJBMF+ygILQSGKQjF4uyJxtgxeTR3MZq7Kh/nqtZWfv/M7bStQAsQkCySYwYaGHGMmbrSxx20FDCnlvCESqxVrTtbXzdrwoiwhXMoTl38TFpDK1ltzmN/qpkVJL1gUVBcrKKFwLIEgQNHeEMI5LTxPHmgnq9/91H+9a1W8dbmCEl0UhGPHQSlM666r16sFI7R/j90z4FaQ8xBVVgYFw2n/AcNh+2T5CftpIyjrDOu5EPzch6x/8QKkpnoudgALoMO2pSEpSDQ0s7KoEXN7Cm8fskkHrvtMRoBi8DR49HYKJ3sqX/9YqI+1sw8BZYCHIVOOuhhIcT14xg3h86NdEx7id9EWwBowsgFiC7iZLTXzRSUO5pEazsrhw3mzPEnsu2Kk5nDlqrrGRK/gDJrFLbuQmkn/8yFNUuWCkaP9TwtSmdnKY2ZsNkzaijzmLrvJW7kLBZgsQInVxFJFAOW8PGPRpqC8o4uPtJAXRibe9uXsoqpHIj/N0Pj52PKMLaO5fWWn4rG6Ow7E+6EVleSQwRDDczmdUZY9/O9wJSMGRXhckWu9US3eQmDgBRg296ny9nIntTNvGeeRcJpYVjV+Wjp4OhEX8yrNPLtnXplScM0aCkxiSfbOGr+gYbwuTSqRQzWN3Eg9RwR9mYORbssKEo8EKEzThIjne01YbCcFNh/ZkHlB+w/egHjg3cSCFXT1rUWgYUhAgNGvgXRElkqHRqNkCamKKcruZMD9qtMqb+XWeWvssRoookKQCHQ4rIBKmKvyNRcJMvbjvJA8hk+6JjH9uijDIlfQNAcQoooWimkYREcSGB684wuwJ4ajRQBDEIcOPxn4vogM+RzzAw8znX8IyA8/uHtfQhRZGKqc6+qwQMogmQ5u7g7eQ/vMJO25FqGxi5ECovOjp391piUohPlkWc2ZhRFImqIEEJJDhivUGaO4TT5DpHAz7g+OAFgc4Sk0ijHIaYLzyYKYtfNZ7CMD9iWvJ41xvkonSLQMcErepYGTDq7DiQR1WXMMgzKHOUWk4pzIzm9pMAUZdhEORB4leGhyzjZeonbgtcFFzI2FKC2MsyJtkOXFqisAAiXfIssZqlM6vEfxHnIeZU18W+wxboRMyhKB+bH7sI7Yhxcs5VzbYfO+irOUoqkUiSywqOLJlCNoU1AEE1up2JQI6OMaxOmNWTfHu7bdZDfDh3EPKExUunD4NgQocQCfrd5PUMb96UeQSd29gCvKFMSaK0RzRHsXy3ktY+2c+mH27hmSDVfqqlkhq3oULhZccnXLkqnSOkuaipmEzTqWbOvifVdp/FI6n+WLWbDzk0seH8LFwmJMaSaMx1FXGnvMPoX0/Y0r+Wk+uSuhQeO+AniiSUcBp64+kHerB/CVY0juD0aZ1M8yV7DoEy4JVJdoN5ro0SccnMMlckpbDy6hIPWszzp7E2re8Q14zjwUtM9rB4+hKZp43nMcWhv7WC1lIQkWIh+1A/TGuImr7pP5CtEJrcRkQjyiSVsD23nR6s3cFpHF5vrqjhbaAxH05n9NLVrOrbuxBRhhsYupDW1ireTJ3Nv6hc82bU3E/C59ZbMWivuoOWX17D0jTVM3L6PZXVVnB00qbUVHeAeWj/rVnog3HWmDhuJoB5fxLt/28F31m3lUsug3OOfhKNJCLw6vYuLV7GLX0BKH2G1eTYfJm5iGRt6Xe/qbGsB4ql/4W+hbdzx3l85vT3G1roqzkYjHE2MT7GVdBOZrtxHIsiHf0jy/ZfZOngWL0S7+N+xDVwXMKlKJNn9905S2xIVgsrURCyrgo32jewsv4/lXZvZjJO5GcxzE+BfKxJBrXuV3XUn8Eo0yfqGWv6ppoJpEsxPWnnh/T96QH9OmvDXYa/5BSf84Hc88NSb6MufYSu/q9fcIW9lAaN63AX1YQNa91zrqvsYftOT3LD0dfSiJ7i2lIjqM2s96rAR5OJlzD39IX7O92tn9xeQgofxS2Z8bxnzMmT6uWy5qvifwgM9WW4MPudPVPlVvt/30X0C6Iv2RfuifdH+X7RiyTFNcv4nk3LluSJ3tTZvHi5yfKaLGJPve/8c6f/VQIAn8wgvBhD8gZ5DFCjI9islEIAOw3EKvi7gYgFzNBhEIrtpbvbXstPvX5Zwg4Q6DZu905kgYYlAnlJN9ZY48XimPuue5DgJN2mYDpwK/AMwy4Sggj30fI4lPWasN2YWsAWI9ZIlrUUzJVwp4FwNU4B2oLU/B5bWlEk+VfS/ru0FYLr/1YCWyKW+ssac7nEVQ3qPsWBGjjU08M1ewKfXOcXXZ1iv74R36pfmmHN2oSQ63xNVCjAk3OB1/DUww4D5Xp/lnnC9Tyju0dGR9GcWluMWf8S7ED3GvlNu0QsT8QFwiYV1qoQ7vM9uAQb30hoCkJ5zEz3L82mNGu7A867schEwVcL9Xr/VQJWPM4svVHltsEDMAo2EpxWsdWAD8HcgDBz1CSQAbYB0PDOdC6IZSHUXy8IVVIgo0R6LWFgiRQrhArAqRepwWjYJI7PJmew+kaocso/yxm9SqKeBQ8oF5jUTtF2g5FIImICEBgew4YjPM72dxXNkQX4uFPUgrwucg+6UyJ8qVLuAUzSQRC8CWvKso7NvzAzZ2GjYBpk7gRZgpX2s7MUBEwER8QYIsLKYmeEDqcfE3bfpUjR3g5Jxq1GiOjss7nCFur7Xbj8utIlsze5+kkzVU08LLb2djco3n8wBjK8UQmevrx3fS+dxi7YvXrDdzWa/kbUyLlKEgPOAqWlyD2OsBOrz8UGOll472EKLziF7yaaUHhRTsBdoBMYCG4EaYL5E1huoF1Lwvh9gAyPpuA+4jQPKgKgBo5xuYk5l0Rjdzam853FXDCCGEwhAXbLbnIqNcaIu6PLUJGoosMMD/BIJjnKdx8FcmpgPGAEccdAvA2cIWKRhqIQTFCxWKBT8imPUyVnrvqvLJPJjhfoYuNI7rhdxuSorX2hX0JuBIxKmqW6ibe0lV+bwNLSVU05nt2KnN7ndRKxNoqZLuF3BewK+quF84Fmgrb/R5iAJD/vjAOm+n5Ynojynd+wg4UFgULaYxIKZeeKYeTnimOm+PsN7fZd+n2IitvjnE/ACMKJQHFPsbwks4CQDo8HB6QK2AvsLEOJwEyZpCDquOf7Vs+1sD5LUAyd5Zqa7OSK8C2J7cshU55mGDXyAy4XZ5h7syk6FxDqcIrUxS99+50rFgCoKfNeXw+nrZ7LEPZWcnIkc8YMukIeJLKaRSwZB9vhC5Rgjff2cIuTwe6t+Z9d9zWrFAMzDAGpdya2YC7cAEPLsX/i8WZkvMs920sNww3nbc9O5VL0iyzwVOWKN9Pw1Xshf4fGFyuEELI+Ya4Bqj28qvTF9vqKVAEGCo0w3O/b3rwHO9Auh0X5QzvUSzIAB86xuDyJ7CR70Ajp6ZsXG2XTnQL1LCScb7toGMNLAOM87vGNKIMEgx0n4igfIMBOGWTA0LYcnc59yJRIksj2jmeWx1MzzgtO9j17xYpcNDsyvgD3R7oCqL+ajvcR1igNP++KmkI9jemhBIoEJrAXWZcLvnjL37WkHn1BJf2gPxI0cqmi4qruhV7S/OdodaxSRBDq5BDayrBsvQL594qRigLENjDEeXzR6tj3JyTHW8XmfJpqUL0cpWlOc3JF41IFdwNeCMN7jMZFnsw4w2gvoxnnyl5Pnl22lAKM1TrUXUNUBtd57wY2uYEUPtSmlVVOdK397G3hXQY2JeZIBl+fZrPKi7bTcQzw+KtiK+VmOpWA97ivddgBnFOta8zzTJYxuDZH++OgoR/MBvzsFuz3LnuSlJ3/KEs1antwf5vGefdOYIEHtQ9nEPZ5wnomjvtwlzUmjcyRtMcc97bAv8BKe14vmCC96H+YhL3TIxle6QPWg78AkSBg+YDRkCjShbHc7I+EvuNX4qZ4KzzUwUsBOXy3ZX5t9F/hnjwcaDGjyvEjS10f4gLkUmOaZxSjcbHmNW0eKZAssx5VTPhQY4x3QyGLiN1GEi6z2bHiv77OyEKG6OPFdORK3AHCyF4AdBDblUOH0/w3ABA+IHbgEm6tvOXCipyUO8FGv0qe/b4V3QNp01Vd4lr3eF7D2KZEsdElViisciMRyIJPTfudC2a5Bi70aJU8i2Nf+x/6ko3g5ipXni5ar/R+7KrjM8xeKkwAAAABJRU5ErkJggg=="">
        <h2 style="position: absolute;
                  left: 140px;
                  top: 20px;
                  text-transform: uppercase;">Cubic House</h2>
      </header>
      <div style="position: absolute;
                  top: 100px;
                  padding: 10px;">
        ${ html }
      </div>
      <footer style="position: absolute;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    padding: 10px;
                    color: #256329;
                    font-size: .8rem;
                    background-color: #95b033;
                    border-top: 2px solid #424d17;">
                    Copyrights 2017 "Cubic House" by modern-living.pl
      </footer>
    `
  }
}
