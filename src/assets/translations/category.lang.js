export const category = {
  "Category is already exists": {
    "pl": "Kategoria już istnieje"
  },
  "Title is required": {
    "pl": "Tytuł jest wymagany"
  },
  "No categories. Please, add new one": {
    "pl": "Brak kategorii. Proszę, dodaj nowy"
  },
  "The requested category does not exist": {
    "pl": "Żądana kategoria nie istnieje"
  },
  "The requested category id is not correct": {
    "pl": "Żądany identyfikator kategorii nie jest poprawny"
  },
  "The category has successfully removed": {
    "pl": "Kategoria została pomyślnie usunięta"
  }
}
