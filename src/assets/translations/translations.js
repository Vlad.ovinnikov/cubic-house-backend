import Localize from 'localize';

import { user } from './user.lang';
import { entry } from './entry.lang';
import { category } from './category.lang';
import { comment } from './comment.lang';
import { profile } from './profile.lang';

export const userLocal = new Localize(user);
export const entryLocal = new Localize(entry);
export const categoryLocal = new Localize(category);
export const commentLocal = new Localize(comment);
export const profileLocal = new Localize(profile);
