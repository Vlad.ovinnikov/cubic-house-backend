export const entry = {
  "Category is not found": {
    "pl": "Nie znaleziono kategorii"
  },
  "There are no posts in this category": {
    "pl": "Brak wpisów w tej kategorii"
  },
  "Title is required": {
    "pl": "Tytuł jest wymagany"
  },
  "Post with that title already exists": {
    "pl": "Post z tym tytułem już istnieje"
  },
  "You have been successfully added new post": {
    "pl": "Dodałeś nowy post"
  },
  "Description is required": {
    "pl": "Opis jest wymagany"
  },
  "Description is shorter than the minimum allowed length 10": {
    "pl": "Opis jest krótszy niż minimalna dozwolona długość 10"
  },
  "Title is shorter than the minimum allowed length 3": {
    "pl": "Tytuł jest krótszy niż minimalna dozwolona długość 3"
  },
  "Entry is not found": {
    "pl": "Nie znaleziono wpisu"
  },
  "User is not found": {
    "pl": "Użytkownik nie został odnaleziony"
  },
  "Entry has been successfully removed": {
    "pl": "Wpis został pomyślnie usunięty"
  }
}
