export const comment = {
  "Text is required": {
    "pl": "Tekst jest wymagany"
  },
  "There is no entry": {
    "pl": "Nie ma wpisu"
  },
  "There is no comment": {
    "pl": "Nie ma komentarza"
  },
  "Comment already exists": {
    "pl": "Komentarz już istnieje"
  },
  "Comment is not found": {
    "pl": "Nie znaleziono komentarza"
  },
  "Comment has been successfully removed": {
    "pl": "Komentarz został pomyślnie usunięty"
  }
}
