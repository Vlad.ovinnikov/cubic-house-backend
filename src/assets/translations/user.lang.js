export const user = {
  "No account with that email address exists": {
    "pl": "Nie istnieje konto z tym adresem e-mail"
  },
  "Cubic House: Password reset": {
    "pl": "Cubic House: Resetowanie hasła"
  },
  "Cubic House: Password updated": {
    "pl": "Cubic House: Hasło zaktualizowane"
  },
  "Cubic House: Request to confirm your account": {
    "pl": "Cubic House: Prośba o potwierdzenie konta"
  },
  "Thank you for registration": {
    "pl": "Dziękujemy za rejestrację"
  },
  "Cubic House: New user account has been created": {
    "pl": "Cubic House: Utworzono nowe konto użytkownika"
  },
  "New user account has been created": {
    "pl": "Utworzono nowe konto użytkownika"
  },
  "To complete the registration process, please activate the Cubic House Blog profile by running the following activation link": {
    "pl": "Aby dokończyć proces rejestracji proszę aktywować profil Cubic House Blog poprzez uruchomienie poniższego linku aktywacyjnego"
  },
  "Warning!": {
    "pl": "Uwaga!"
  },
  "The activation link is valid for 24 hours. After this time to activate the profile, please send an email to info.cubichouse@gmail.com with the information in the subject line \"Activation Cubic House Blog\"": {
    "pl": "Link aktywacyjny jest ważny przez 24 godziny. Po upływie tego czasu w celu aktywacji profilu prosimy o dokonanie zgłoszenia na adres e-mail info.cubichouse@gmail.com z informacją w temacie wiadomości \"Aktywacja Cubic House Blog\""
  },
  "Confirm account": {
    "pl": "Potwierdź konto"
  },
  "Confirm email": {
    "pl": "Potwierdź email"
  },
  "Hello": {
    "pl": "Witaj"
  },
  "Reset password": {
    "pl": "Zresetowanie hasła"
  },
  "You are receiving this because you (or someone else) have requested the reset of the password for your account": {
    "pl": "Otrzymujesz to, ponieważ (lub inni) zażądali zresetowania hasła dla Twojego konta"
  },
  "Please click on the following link, or paste this into your browser to complete the process:": {
    "pl": "Kliknij poniższy link lub wklej ten plik do przeglądarki, aby dokonać proces:"
  },
  "If you did not request this, please ignore this email and your password will remain unchanged": {
    "pl": "Jeśli nie zażądałeś tego, zignoruj ten e-mail, a Twoje hasło pozostanie niezmienione"
  },
  "Authentication failed. User does not exist": {
    "pl": "Uwierzytelnianie nie powiodło się. użytkownik nie istnieje"
  },
  "Authentication failed. Password is not correct": {
    "pl": "Uwierzytelnianie nie powiodło się. Hasło jest nieprawidłowe"
  },
  "Authentication failed. You can not login before email is confirmed. We have been sent to you a new email with confirmation link": {
    "pl": "Uwierzytelnianie nie powiodło się. Nie możesz się zalogować, zanim potwierdzisz wiadomość. Zostaliśmy wysłani do Ciebie nowy e-mail z linkiem potwierdzającym"
  },
  "User already exist. Please, use another email or just login if it's yours": {
    "pl": "Użytkownik już istnieje"
  },
  "Password is shorter than the minimum allowed length 6": {
    "pl": "Hasło jest krótsze niż minimalna dozwolona długość 6"
  },
  "Password is required": {
    "pl": "Wymagane jest hasło"
  },
  "Email is required": {
    "pl": "Wymagane jest email"
  },
  "Email is not valid": {
    "pl": "Email jest nieprawidłowy"
  },
  "An email has been sent to": {
    "pl": "Wysłano e-maila do"
  },
  "with further instructions": {
    "pl": "z dalszymi instrukcjami"
  },
  "Password reset token is invalid or has expired": {
    "pl": "Token z resetem hasła jest nieprawidłowy lub wygasł"
  },
  "This is a confirmation that the password for your account": {
    "pl": "To jest potwierdzenie, że hasło konta"
  },
  "has just been changed": {
    "pl": "zostało właśnie zmienione"
  },
  "Success! Your password has been changed": {
    "pl": "Powodzenie! Twoje hasło zostało zmienione"
  },
  "Your email is successfully confirmed": {
    "pl": "Twój email został z powodzeniem potwierdzony"
  },
  "Now you can login to your blog account": {
    "pl": "Teraz możesz zalogować się na swoim konte blogu"
  },
  "Confirm account token is invalid or has expired": {
    "pl": "Potwierdź token konta jest nieprawidłowy lub wygasł"
  },
  "Cubic House: Confirmation account": {
    "pl": "Cubic House: Potwerdzenie konta"
  },
  "Email has been successfully updated": {
    "pl": "Email został pomyślnie zaktualizowany"
  },
  "User not found": {
    "pl": "Użytkownik nie znaleziony"
  },
  "Password has been successfully updated": {
    "pl": "Hasło zostało pomyślnie zaktualizowane"
  }
};
