//-- Exports --//
export const USER_ROLE = {
  USER: 'USER',
  ADMIN: 'ADMIN',
  SUPER_ADMIN: 'SUPER_ADMIN'
};
// salt factor
export const SALT_WORK_FACTOR = 10;
// validation errors
export const ERRORS = {
  MIN: 'minlength',
  MAX: 'maxlength',
  REQUIRED: 'required',
  VALIDATOR: 'ValidatorError'
};

export const PRIORITY = {
  NONE: 0,
  LOW: 1,
  HIGH: 2
};

export const STATUS = {
  DRAFT: 0,
  PUBLISHED: 1,
  ARCHIVED: 2
};

export const AUTH_HEADER = 'x-auth';
export const LANG_HEADER = 'x-lang';
