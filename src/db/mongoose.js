//-- Dependencies --//
// lib dependencies
import mongoose from 'mongoose';
// app dependencies
import dbConfig from './../configs/db.config';

//-- Implementation --//
mongoose.Promise = global.Promise;

function connectToDatabase() {
  let dbConnection;

  if(process.env.NODE_ENV === 'test') {
    dbConnection = process.env.MONGODB_URI;

  } else if(process.env.NODE_ENV === 'development') {
    dbConnection = `mongodb://${ process.env.MONGODB_USER }:${ process.env.MONGODB_PASSWORD }${ process.env.MONGODB_URI }`;

  } else {
    dbConnection = `mongodb://${ process.env.MONGODB_USER }:${ process.env.MONGODB_PASSWORD }${ process.env.MONGODB_URI }`;
  }

  if(process.env.NODE_ENV !== 'test') {
    console.log(`User     = ${process.env.MONGODB_USER}`);
    console.log(`Password = ${process.env.MONGODB_PASSWORD}`);
    console.log(`EMAIL    = ${ process.env.EMAIL }`);
    console.log(`EMAIL_PASSWORD = ${ process.env.EMAIL_PASSWORD }`);
  }

  console.log(`EMAIL    = ${ process.env.EMAIL }`);
  console.log(`EMAIL_PASSWORD = ${ process.env.EMAIL_PASSWORD }`);
  console.log(`NODE_ENV    = ${ process.env.NODE_ENV }`);
  console.log(`MONGODB_URI = ${ process.env.MONGODB_URI }`);

  return mongoose.connect(dbConnection, (err) => {
      if (!err) {
          console.log(`Connected to MongoDb server: ${ dbConnection }`);

      } else {
          console.error(`Unable to connect to MongoDb server.
            Error: ${err}`);
      }
  }).connection;
}
connectToDatabase()
  .on('error', console.error.bind(console))
  .on('disconnected', connectToDatabase)

//-- Exports --//
export default { mongoose };
