//-- Dependencies --//
// lib dependencies
import _ from 'underscore';
import crypto from 'crypto';
import moment from 'moment';
import { waterfall } from 'async';

// app dependencies
import { User, UserProfile } from './../models/_index';
import { AUTH_HEADER, LANG_HEADER, USER_ROLE } from './../configs/constants.config';
import { ERRORS } from './../configs/constants.config';
import { userLocal } from './../assets/translations/translations';
import { MailGenerator, SMTP_TRANSPORT } from './../assets/helpers/mailGenerator';

/**
 * signUp user
 * @param  {[type]} req [request param]
 * @param  {[type]} res [reponse param]
 * @return {[type]}     [return user with auth token or error message]
 */
function signup(req, res, next) {
  let lang = req.header(LANG_HEADER);
  userLocal.setLocale(lang);

  let body = _.pick(req.body, ['email', 'password']);
  let user = new User({ email: body.email, password: { hash: body.password } });

  waterfall([
    (done) => {
      _randomBytes()
        .then((token) => {
          done(null, token);
        })
        .catch((err) => done(err));
    },
    (token, done) => {
      User.findOne({ email: body.email }, (err, userExist) => {
        if(err) { done(err); }

        if(userExist) {
          // done();
          return res.status(400).send({ message: userLocal.translate('User already exist. Please, use another email or just login if it\'s yours') });
          done();
        }

        const access = 'emailConfirm';
        const expiresIn = moment().add(1, 'days');
        user.tokens.push({ access, token, expiresIn });

        user.save((err) => {
          done(err, token, user);
        });
      });
    },
    (token, user, done) => {
      let profile = new UserProfile({ _userId: user._id });
      profile.firstName = body.email.split('@')[0];
      profile.lastName = 'Doe';
      profile.gender = true;
      profile.birthdate = new Date();
      profile.avatar = null;
      profile.save((err) => {
        done(err, token, user);
      });
    },
    (token, user, done) => {
      let mailGenerator = new MailGenerator (
        user.email,
        process.env.EMAIL,
        userLocal.translate('Cubic House: Request to confirm your account'),
        `
          <h2>${userLocal.translate('Hello')}</h2>
          <h3>${userLocal.translate('Thank you for registration')}</h3>
          <p>${userLocal.translate('To complete the registration process, please activate the Cubic House Blog profile by running the following activation link')}</p>
          <p>
            <span>Login: </span><span style="font-weight: bold">${user.email}</span>
          </p>
          <p style="width: 100%;
                    padding: 30px;
                    margin-bottom: 30px;
                    background-color: #f0f5de;">
            <a href="${req.headers.origin}/enter/email-confirm/${token}">${userLocal.translate('Confirm email')}</a>
          </p>
          <p style="font-weight: bold;">${userLocal.translate('Warning!')}</p>
          <p>${userLocal.translate('The activation link is valid for 24 hours. After this time to activate the profile, please send an email to info.cubichouse@gmail.com with the information in the subject line \"Activation Cubic House Blog\"')}</p>
        `
      );

      SMTP_TRANSPORT.sendMail(mailGenerator, (err) => {
        if(err) {

          UserProfile.findOneAndRemove({ _userId: user._id })
            .then(() => {

              User.findOneAndRemove({ _id: user._id })
                .then(() => {
                  done(err);
                });
            });
        } else {

          done(null, user);
        }
      });

    },
    (user, done) => {
      User.findOne({ role: USER_ROLE.SUPER_ADMIN}, (err, superAdmin) => {

        let mailGenerator = new MailGenerator (
          superAdmin.email,
          process.env.EMAIL,
          userLocal.translate('Cubic House: New user account has been created'),
          `
            <h2>${userLocal.translate('Hello')}</h2>
            <h3>${userLocal.translate('New user account has been created')}:</h3>
            <p>
              <span>Id: </span><span style="font-weight: bold">${ user._id }</span>
            </p>
            <p>
              <span>Email: </span><span style="font-weight: bold">${ user.email }</span>
            </p>
            <p>
              <span>Created at: </span><span style="font-weight: bold">${ user.createdAt }</span>
            </p>
          `
        );

        SMTP_TRANSPORT.sendMail(mailGenerator, (err) => {
          if(err) { done(err) }

          res.status(200).send({ message: userLocal.translate('An email has been sent to') + ' ' + user.email + ' ' +  userLocal.translate('with further instructions') });
          done(null);
        });
      });
    }
  ], (err) => {
    if(err) { return next(err); }
    res.send(err);
  });
}

/**
 * account confirm
 * @param  {[type]} req
 * @param  {[type]} res
 * @return {[type]}
 */
function accountConfirm(req, res) {
  let token = req.body.token;

  let lang = req.header(LANG_HEADER);
  userLocal.setLocale(lang);

  waterfall([
    (done) => {
      User.findOne({
        'tokens.access': 'emailConfirm',
        'tokens.token': token,
        'tokens.expiresIn': { $gt: moment().format() }
      })
      .then((user) => {
        if(!user) {
          return res.status(400)
                    .send({ message: userLocal.translate('Confirm account token is invalid or has expired') });
        }

        user.removeToken(token)
          .then(() => {
            done(null, user);
          }, (err) => {
            done(err);
          });
      });
    },
    (user, done) => {
      let mailGenerator = new MailGenerator (
        user.email,
        process.env.EMAIL,
        userLocal.translate('Cubic House: Confirmation account'),
        `
          <h2>${userLocal.translate('Hello')}</h2>
          <p>${userLocal.translate('Your email is successfully confirmed')}</p>
          <p>${userLocal.translate('Now you can login to your blog account')}</p>
        `
      );

      SMTP_TRANSPORT.sendMail(mailGenerator, (err) => {
        if(err) { return next(err); }

        res.status(200).send({ message: userLocal.translate('Your email is successfully confirmed'), user: user });
        done(null);
      });
    }
  ], (err) => {
    if(err) { return next(err); }
    res.send(err);
  });
}

/**
 * login user and generate auth token
 * @param  {[type]} req [request param]
 * @param  {[type]} res [rsponse param]
 * @return {[type]}     [if no error return user and auth header]
 */
function login(req, res) {
  let lang = req.header(LANG_HEADER);
  userLocal.setLocale(lang);

  let body = _.pick(req.body, ['email', 'password']);

  User.findByCredentials(body.email, body.password, req)
    .then((user) => {

      return user.generateAuthToken(req)
        .then((token) => {
          UserProfile.findOne({ _userId: user._id })
            .then((profile) => {
              res.send({ user, profile, token });
            });
        });
    })
    .catch((err) => {
      res.status(400).send({ message: userLocal.translate(err) });
    });
}

/**
 * logout user and remove token
 * @param  {[type]} req
 * @param  {[type]} res
 * @return {[type]}
 */
function logout(req, res) {
  req.user.removeToken(req.token)
    .then((user) => {
      res.status(200).send(user);
    }, () => {
      res.status(400).send();
    });
}
/**
 * forgot password
 * generate random string for reseting password
 * @param {[type]}   req
 * @param {[type]}   res
 * @param {Function} next
 */
function forgotPassword(req, res, next) {
  let lang = req.header(LANG_HEADER);
  userLocal.setLocale(lang);

  waterfall([
    (done) => {
      _randomBytes()
        .then((token) => {
          done(null, token);
        })
        .catch((err) => {
          done(err);
        });
    },
    (token, done) => {
      User.findOne({ email: req.body.email }, (err, user) => {
        if(err) { done(err); }

        if(!user) {
          return res.status(400).send({ message: userLocal.translate('No account with that email address exists') });
        }

        const access = 'passwordReset';
        const expiresIn = moment().add(1, 'hours');
        user.tokens.push({ access, token, expiresIn });

        user.save((err) => {
          done(err, token, user);
        });
      });
    },
    (token, user, done) => {

      let mailGenerator = new MailGenerator (
        user.email,
        process.env.EMAIL,
        userLocal.translate('Cubic House: Password reset'),
        `
          <h2>${userLocal.translate('Hello')}</h2>
          <p>${userLocal.translate('You are receiving this because you (or someone else) have requested the reset of the password for your account')}</p>
          <p>${userLocal.translate('Please click on the following link, or paste this into your browser to complete the process:')}</p>
          <p>
            <a href="${req.headers.origin}/enter/reset/${token}">${userLocal.translate('Reset password')}</a>
          </p>
          <p>${userLocal.translate('If you did not request this, please ignore this email and your password will remain unchanged')}</p>
        `
      );

      SMTP_TRANSPORT.sendMail(mailGenerator, (err) => {
        if(err) { done(err); }

        res.status(200).send({ message: userLocal.translate('An email has been sent to') + ' ' + user.email + ' ' +  userLocal.translate('with further instructions') });
        done(null);
      });
    }
  ], (err) => {
    if(err) { return next(err); }
    res.send(err);
  });
}

function resetPassword(req, res, next) {
  let token = req.params.token;
  let lang = req.header(LANG_HEADER);
  userLocal.setLocale(lang);

  waterfall([
    (done) => {
      User.findOne({
        'tokens.access': 'passwordReset',
        'tokens.token': token,
        'tokens.expiresIn': { $gt: moment().format() }
      })
      .then((user) => {
        if(!user) {
          return res.status(400)
                    .send({ message: userLocal.translate('Password reset token is invalid or has expired') });
        }

        user.removeToken(token)
          .then(() => {
            user.password.hash = req.body.password;
            user.save((err) => {
              done(err, user);
            });
          }, () => {
            done(err);
          });
      });
    },
    (user, done) => {

      let mailGenerator = new MailGenerator (
        user.email,
        process.env.EMAIL,
        userLocal.translate('Cubic House: Password updated'),
        `
          <h2>${userLocal.translate('Hello')}</h2>
          <p>
            ${userLocal.translate('This is a confirmation that the password for your account')}
            ${user.email}
            ${userLocal.translate('has just been changed')}
          </p>
        `
      );

      SMTP_TRANSPORT.sendMail(mailGenerator, (err) => {
        if(err) { done(err); }

        res.status(200).send({ message: userLocal.translate('Success! Your password has been changed') });
        done(null);
      });
    }
  ], (err) => {
    if(err) { return next(err); }
    res.send(err);
  });
}

function updateEmail(req, res) {
  let lang = req.header(LANG_HEADER);
  userLocal.setLocale(lang);

  let email = _.pick(req.body, ['email']);
  User.findOneAndUpdate({ _id: req.user._id }, { $set: email }, { new: true })
    .then((user) => {
      if (!user) {
        return res.send({ message: userLocal.translate('User not found')});
      }

      res.status(200).send({ user: user, message: userLocal.translate('Email has been successfully updated') });
    });
}

function updatePassword(req, res) {
  let lang = req.header(LANG_HEADER);
  userLocal.setLocale(lang);
  let body = _.pick(req.body, ['password']);

  waterfall([
    (done) => {
      User.findOne({ _id: req.user._id })
        .then((user) => {
              if (!user) {
                return res.status(400).send({ message: userLocal.translate('User not found')});
              }
              user.password.hash = body.password;
              user.generateAuthToken(req)
                .then((token) => {
                  res.status(200).send({ user: user, token: token, message: userLocal.translate('Password has been successfully updated') });

                  req.user.removeToken(req.token)
                    .then((user) => {
                      res.status(200).send(user);
                    }, () => {
                      res.status(400).send();
                    });
                });

        });
    },
    (done) => {

    }
  ])

}

function _randomBytes() {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(20, (err, buf) => {
      if(buf) {
        let token = buf.toString('hex');
        resolve(token);
      } else {
        reject(err);
      }
    });
  })
}

//-- Exports --//
export default { signup, accountConfirm, login, logout, forgotPassword, resetPassword, updateEmail, updatePassword };
