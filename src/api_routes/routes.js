//-- Dependencies --//
// lib dependencies
import express from 'express';
import cors from 'cors';

// app dependencies
import { USER_ROLE } from './../configs/constants.config';
import { authenticateWithRoles } from './../middleware/authenticate';
import { userAPI, categoryAPI, entryAPI, commentAPI, profileAPI } from './_index';

/**
 * routes
 * @param  {[type]} app app dependency
 */
function routes(app) {
  // API routes
  let router = express.Router();
  app.use('/api', router);

  app.options('*', cors());

  //-- User
  router.post('/signup', userAPI.signup);
  router.post('/login', userAPI.login);
  router.delete('/logout', authenticateWithRoles(''), userAPI.logout);
  router.post('/forgot', userAPI.forgotPassword);
  router.post('/reset/:token', userAPI.resetPassword);
  router.post('/email-confirm', userAPI.accountConfirm);

  //-- Profile
  router.get('/user/profile', authenticateWithRoles(''), profileAPI.get);
  router.put('/user/profile', authenticateWithRoles(''), profileAPI.update);
  router.put('/user/email', authenticateWithRoles(''), userAPI.updateEmail);
  router.put('/user/password', authenticateWithRoles(''), userAPI.updatePassword);

  //-- Category
  router.get('/categories', authenticateWithRoles(''), categoryAPI.getList);
  router.get('/categories/:id', authenticateWithRoles(''), categoryAPI.get);
  router.post('/categories', authenticateWithRoles([USER_ROLE.ADMIN, USER_ROLE.SUPER_ADMIN]), categoryAPI.add);
  router.put('/categories/:id', authenticateWithRoles([USER_ROLE.ADMIN, USER_ROLE.SUPER_ADMIN]), categoryAPI.update);
  router.delete('/categories/:id', authenticateWithRoles([USER_ROLE.ADMIN, USER_ROLE.SUPER_ADMIN]), categoryAPI.remove);

  //-- Entry
  router.get('/categories/:id/entries', authenticateWithRoles(''), entryAPI.getListWithCategory);
  router.get('/entries', authenticateWithRoles(''), entryAPI.getList);
  router.get('/entries/recent', authenticateWithRoles(''), entryAPI.getRecent);
  router.get('/user/entries', authenticateWithRoles(''), entryAPI.getUsersList);
  router.get('/entries/:id', authenticateWithRoles(''), entryAPI.get);
  router.post('/entries', authenticateWithRoles(''), entryAPI.add);
  router.put('/entries/:id', authenticateWithRoles(''), entryAPI.update);
  router.delete('/entries/:id', authenticateWithRoles(''), entryAPI.remove);

  //-- Comment
  router.post('/comments', authenticateWithRoles(''), commentAPI.add);
  router.get('/entries/:id/comments', authenticateWithRoles(''), commentAPI.getListWithEntryId);
  router.get('/comments/recent', authenticateWithRoles(''), commentAPI.getRecent);
  router.put('/comments/:id', authenticateWithRoles(''), commentAPI.update);
  router.delete('/entries/:entryId/comments/:commentId', authenticateWithRoles(''), commentAPI.remove);

}

//-- Exports --//
export { routes };
