//-- Dependencies --//
//-- lib dependencies
import _ from 'underscore';
import { ObjectID } from 'mongodb';
import { waterfall } from 'async';

//-- app dependencies
import { Entry, UserProfile, Comment } from './../models/_index';
import { ERRORS } from './../configs/constants.config';
import { entryLocal } from './../assets/translations/translations';
import { LANG_HEADER } from './../configs/constants.config';

/**
 * add new entry with category's and users id
 * @param {[type]} req [request param]
 * @param {[type]} res [response param]
 */
function add(req, res) {
  let lang = req.header(LANG_HEADER);
  entryLocal.setLocale(lang);

  let body = _.pick(req.body, ['title', 'description', 'priority', 'status', 'category']),
      entry = new Entry({
        title: body.title,
        description: body.description,
        priority: body.priority,
        status: body.status,
        category: body.category
      });

  if(!entry.title) {
    return res.status(400).send({ message: entryLocal.translate('Title is required') });
  }

  Entry.findOne({ title: entry.title })
    .then((existingEntry) => {
      if(existingEntry) {
        return res.status(400).send({ message: entryLocal.translate('Post with that title already exists') });

      } else {
        _checkId(res, req.user._id, 'User is not found');

        UserProfile.findOne({ _userId: req.user._id })
          .then((profile) => {

            entry.author = profile._id
            entry.save()
              .then((data) => {
                res.send({
                  message: entryLocal.translate('You have been successfully added new post'),
                  entry: data
                });
              })
              .catch((err) => {
                if(err.errors) {
                  if(err.errors['description'].properties.type === ERRORS.REQUIRED) {
                    return res.status(400).send({ message: entryLocal.translate('Description is required') });
                  } else if(err.errors['description'].properties.type === ERRORS.MIN) {
                    return res.status(400).send({ message: entryLocal.translate('Description is shorter than the minimum allowed length 10') });
                  } else if(err.errors['title'].properties.type === ERRORS.MIN) {
                    return res.status(400).send({ message: entryLocal.translate('Title is shorter than the minimum allowed length 3') });
                  } else {
                    res.status(400).send(err);
                  }

                } else {
                  return res.status(400).send(err);
                }
              });
          });
      }
    });
}

/**
 * get list of entries with category id
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of entries or error]
 */
function getListWithCategory(req, res) {
  let lang = req.header(LANG_HEADER);
  entryLocal.setLocale(lang);

  const status = 1;
  let category = req.params.id;
  let perPage = parseInt(req.query.perPage, 10);
  let page = req.query.page > 0 ? parseInt(req.query.page, 10) : 0;

  _checkId(res, category, 'Category is not found');

  Entry.find({ category, status })
    .limit(perPage)
    .skip(perPage * page)
    .sort({ updatedAt: 'desc' })
    .populate('author category')
    .then((entries) => {
      if(!entries || !entries.length) {
        return res.status(404).send({ message: entryLocal.translate('There are no posts in this category') });
      }

      Entry.find().count((err, count) => {
        let pages = Math.round(count)/perPage;
        res.send({ entries, page, pages });
      });
    })
    .catch((err) => {
      res.send(err);
    });
}

/**
 * get list of entries 10 by page
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of entries or error]
 */
function getList(req, res) {
  let lang = req.header(LANG_HEADER);
  entryLocal.setLocale(lang);

  let perPage = parseInt(req.query.perPage, 10);
  let page = req.query.page > 0 ? parseInt(req.query.page, 10) : 0;

  Entry.find()
    .limit(perPage)
    .skip(perPage * page)
    .sort({ updatedAt: 'desc' })
    .populate('author category comments')
    .then((entries) => {

      Entry.find().count((err, count) => {
        let pages = Math.round(count)/perPage;
        res.send({ entries, page, pages });
      });
    })
    .catch((err) => {
      res.send(err);
    });
}

/**
 * get list of entries posted by user
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of entries or error]
 */
function getUsersList(req, res) {
  let lang = req.header(LANG_HEADER);
  entryLocal.setLocale(lang);

  let perPage = parseInt(req.query.perPage, 10);
  let page = req.query.page > 0 ? parseInt(req.query.page, 10) : 0;
  let category = req.query.category;

  waterfall([
    (done) => {
      UserProfile.findOne({ _userId: req.user._id})
        .then((profile) => {
          done(null, profile);
        })
        .catch((err) => done(err));
    },
    (profile, done) => {
      if(category) {
        _checkId(res, category, 'Category is not found');
        Entry.find({ author: profile._id, category: category })
          .limit(perPage)
          .skip(perPage * page)
          .sort({ updatedAt: 'desc' })
          .populate('author category comments')
          .then((entries) => {
            done(null, entries);
          })
          .catch((err) => {
            done(err);
          });
      } else {
        Entry.find({ author: profile._id })
          .limit(perPage)
          .skip(perPage * page)
          .sort({ updatedAt: 'desc' })
          .populate('author category comments')
          .then((entries) => {
            done(null, entries);
          })
          .catch((err) => {
            done(err);
          });
      }

    },
    (entries, done) => {
      Entry.find().count((err, count) => {
        let pages = Math.round(count)/perPage;
        if(!entries || !entries.length) {
          return res.status(404).send({ message: entryLocal.translate('There are no posts in this category') });
        }
        res.status(200).send({ entries, page, pages });
      });
    }
  ],
    (err) => {
      res.send(err);
    }
  );
}

/**
 * get 5 recent entries
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of entries or error]
 */
function getRecent(req, res) {
  let lang = req.header(LANG_HEADER);
  entryLocal.setLocale(lang);

  Entry.find()
    .sort({ updatedAt: 'desc' })
    .limit(5)
    .populate('author category comments')
    .then((entries) => {
      res.send(entries);
    })
    .catch((err) => {
      res.send(err);
    });
}

/**
 * get entry by its id
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return entry or error]
 */
function get(req, res) {
  let id = req.params.id;

  _checkId(res, id, 'Entry is not found');

  Entry.findById(id)
    .populate('author category')
    .then((entry) => {
      if(!entry) {
        return res.status(404).send({ message: entryLocal.translate('Entry is not found') });
      }

      res.send(entry);
    })
    .catch((err) => {
      res.send(err);
    });
}

/**
 * update entry by its id and only owner can do it
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return entry if no error]
 */
function update(req, res) {
  let id = req.params.id,
      body = _.pick(req.body, ['title', 'description', 'priority', 'status', 'category', 'author']);

  _checkId(res, id, 'Entry is not found');

  Entry.findOneAndUpdate({ _id: id, author: body.author._id }, { $set: body }, { new: true })
    .then((entry) => {
      if(!entry) {
        return res.status(404).send({ message: entryLocal.translate('Entry is not found') });
      }

      res.send(entry);
    })
    .catch((err) => res.send(err) );
}

/**
 * remove entry by its id and only owner can do it
 * @param  {[type]} req [requset param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [description]
 */
function remove(req, res) {
  let id = req.params.id;

  _checkId(res, id, 'Entry is not found');

  waterfall([
    (done) => {
      UserProfile.findOne({ _userId: req.user._id })
        .then((profile) => {
          done(null, profile);
        })
        .catch((err) => done(err));
    },
    (profile, done) => {
      Entry.findOne({ _id: id, author: profile._id })
        .then((entry) => {
          if(!entry) {
            return res.status(404).send({ message: entryLocal.translate('Entry is not found') });
          }
          done(null, entry);
        })
        .catch((err) => res.send(err));
    },
    (entry, done) => {
      entry.remove()
        .then(() => {
          res.status(200).send({ message: entryLocal.translate('Entry has been successfully removed') });
        })
        .catch((err) => res.send(err));
    }
  ], (err) => {
    res.send(err);
  });

}

function _checkId(res, id, message) {
  if (!ObjectID.isValid(id)) {
    return res.status(404).send({ message: entryLocal.translate(message) });
  }
}

//-- Exports --//
export default { add, getList, getUsersList, getListWithCategory, get, update, remove, getRecent };
