//-- Dependencies --//
// lib dependencies
import _ from 'underscore';
import { ObjectID } from 'mongodb';
import { waterfall } from 'async';
// app dependencies
import { Category, UserProfile } from './../models/_index';
import { categoryLocal } from './../assets/translations/translations';
import { LANG_HEADER } from './../configs/constants.config';

/**
 * add new category only can do admin or superAdmin
 * @param {[type]} req [request param]
 * @param {[type]} res [response param]
 */
function add(req, res) {
  let lang = req.header(LANG_HEADER);
  categoryLocal.setLocale(lang);

  let body = _.pick(req.body, ['title', 'description']),
      category = new Category({ title: body.title, description: body.description });

  if(!category.title) {
    return res.status(400).send({ message: categoryLocal.translate('Title is required') });
  }

  waterfall([
    (done) => {
      UserProfile.findOne({ _userId: req.user._id })
        .then((userProfile) => {
          category.author = userProfile._id;
          done(null, userProfile);
        })
        .catch((err) => done(err));
    },
    (userProfile, done) => {
      Category.findOne({ title: category.title })
        .then((existingCategory) => {

          if(existingCategory) {
            return res.status(400).send({ 'message': categoryLocal.translate('Category is already exists') });
          }
          done();
        })
        .catch((err) => done(err));
    },
    (done) => {

      category.save()
        .then((data) => {
          data.populate('author', (err) => {
            if(err) {
              return res.status(400).send(err);
            }

            res.status(200).send(data);
          });
        })
        .catch((err) => {
          res.status(400).send(err);
        });
    }
  ],
  (err) => {
    res.send(err);
  }
);

}

/**
 * get list of categories
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of categories or error]
 */
function getList(req, res) {
  let lang = req.header(LANG_HEADER);
  categoryLocal.setLocale(lang);

  Category.find()
    .sort({ 'title': 'asc' })
    .populate('author')
    .then((categories) => {

      if(!categories) {
        return res.status(404).send({ 'message': categoryLocal.translate('No categories. Please, add new one') });
      }

      res.status(200).send(categories);
    })
    .catch((err) => {
      res.send(err);
    });
}

/**
 * get category by its id
 * @param  {[type]} req [reqiest param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [if id is valid and no another error return category]
 */
function get(req, res) {
  let lang = req.header(LANG_HEADER);
  categoryLocal.setLocale(lang);

  let id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send({ 'message': categoryLocal.translate('The requested category id is not correct') });
  }

  Category.findOne({ _id: id })
    .populate('author')
    .then((category) => {
      if(!category) {
        return res.status(404).send({ 'message': categoryLocal.translate('The requested category does not exist') });
      }
      res.send(category);
    })
    .catch((err) => {
      res.status(400).send(err);
    });
}

/**
 * update category by id
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [if no error returns updated category]
 */
function update(req, res) {
  let lang = req.header(LANG_HEADER);
  categoryLocal.setLocale(lang);

  let id = req.params.id,
      body = _.pick(req.body, ['title', 'description']);

  if (!ObjectID.isValid(id)) {
    return res.status(404).send({ 'message': categoryLocal.translate('The requested category id is not correct') });
  }

  Category.findByIdAndUpdate(id, {$set: body}, { new: true })
    .populate('author')
    .then((category) => {
      if(!category) {
        return res.status(404).send({ 'message': categoryLocal.translate('The requested category does not exist') });
      }

      res.status(200).send(category);
    })
    .catch((err) => {
      res.status(400).send(err);
    });
}

/**
 * remove entry
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [if no error return removed entry]
 */
function remove(req, res) {
  let lang = req.header(LANG_HEADER);
  categoryLocal.setLocale(lang);

  let id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send({ 'message': categoryLocal.translate('The requested category id is not correct') });
  }

  waterfall([
    (done) => {
      Category.findOne({ _id: id })
        .then((category) => {
          if(!category) {
            return res.status(404).send({ message: categoryLocal.translate('The requested category does not exist') });
          }
          done(null, category);
        })
        .catch((err) => done(err));
    },
    (category, done) => {
      category.remove()
        .then((data) => {
          res.status(200).send({ message: categoryLocal.translate('The category has successfully removed') });
        })
        .catch((err) => res.send(err));
    }
  ],
  (err) => {
    res.send(err);
  });
}

//-- Exports --//
export default { add, getList, get, update, remove };
