//-- Dependencies --//
//-- lib dependencies
import _ from 'underscore';
import moment from 'moment';
// app dependencies
import { UserProfile } from './../models/_index';
import { profileLocal } from './../assets/translations/translations';
import { LANG_HEADER } from './../configs/constants.config';

/**
 * get user profile
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 */
function get(req, res) {
  let user = req.user;
  UserProfile.findOne({ _userId: user._id })
    .then((profile) => {
      res.status(200).send({ profile, user });
    })
    .catch((err) => {
      res.send(err);
    })
}

function update(req, res) {
  let lang = req.header(LANG_HEADER);
      profileLocal.setLocale(lang);

  let body = _.pick(req.body, ['firstName', 'lastName', 'avatar', 'gender', 'birthdate']);

  if (body.birthdate) {
    body.birthdate = moment(new Date(body.birthdate));
  }

  UserProfile.findOneAndUpdate({ _userId: req.user._id }, { $set: body }, { new: true })
    .then((profile) => {
      if(!profile) {
        return res.status(400).send({ message: profileLocal.translate('Profile not found')});
      }

      res.status(200).send({ profile: profile, message: profileLocal.translate('Profile has been successfully updated') });
    })
}

export default { get, update };
