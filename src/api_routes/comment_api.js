//-- Dependencies --//
// lib dependencies
import _ from 'underscore';
import { ObjectID } from 'mongodb';
import { waterfall } from 'async';
// app dependencies
// import Comment from './../models/comment'
import { Entry, Comment, UserProfile } from './../models/_index';
import { commentLocal } from './../assets/translations/translations';
import { LANG_HEADER } from './../configs/constants.config';

/**
 * add new commment
 * @param {[type]} req [request param]
 * @param {[type]} res [response param]
 */
function add(req, res, next) {
  let lang = req.header(LANG_HEADER);
  commentLocal.setLocale(lang);

  let body = _.pick(req.body, ['text', 'parentId', 'vote', 'entry']),
      commentBody = new Comment({
        text: body.text,
        entry: body.entry
      });

  // check if entry id is valiud
  _checkId(res, commentBody.entry, 'There is no entry');
  // check if parent comment id is valid
  if (body.parentId) {
    commentBody.parentId = body.parentId;
    _checkId(res, commentBody.parentId, 'There is no comment');
  }

  if(!commentBody.text) {
    return res.status(400).send({ message: commentLocal.translate('Text is required') });
  }

  waterfall([
    // check if entry exists
    (done) => {
      Entry.findOne({ _id: commentBody.entry })
        .then((entry) => {
          if(!entry) {
            return res.status(404).send({ message: commentLocal.translate('There is no entry') });
          }
          done(null, entry);
        })
        .catch((err) => done(err));
    },
    // find user profile by its id
    (entry, done) => {
      UserProfile.findOne({ _userId: req.user._id })
        .then((userProfile) => {

          commentBody.author = userProfile._id;
          done(null, commentBody, entry);
        })
        .catch((err) => done(err));
    },
    // save new comment to db
    (comment, entry, done) => {
      comment.save()
        .then((newComment) => {

          done(null, newComment, entry);
        })
        .catch((err) => done(err));
    },
    (comment, entry, done) => {

      if(comment.parentId) {
        Comment.findOne({ _id: comment.parentId })
          .then((parentComment) => {

            if(!parentComment) {
              return res.status(404).send({ message: commentLocal.translate('There is no comment') });
            }

            parentComment.children.push(comment._id);
            parentComment.save()
              .then(() => {
                done(null, comment, entry);
              })
              .catch((err) => done(err));
          });
      } else {
        done(null, comment, entry);
      }
    },
    // find entry by id and save new comment
    (comment, entry, done) => {

      Entry.findOneAndUpdate({ _id: entry._id }, { $push: { comments: comment._id } })
        .then((data) => {
          res.status(200).send(comment);
        })
        .catch((err) => done(err));
    }
  ],
    (err) => {
      res.send(err);
    }
  );
}

/**
 * get list comments by entry id
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
function getListWithEntryId(req, res) {
  let entryId = req.params.id;

  _checkId(res, entryId, 'There is no entry');

  Comment.find({ entry: entryId, parentId: null })
    .populate('author children')
    .then((comments) => {
      let options = {
        path: 'children.author',
        model: 'UserProfile'
      };
      Comment.populate(comments, options)
        .then((popComments) => {
          res.status(200).send(popComments);
        })
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));
}

/**
 * get 5 recent comments
 * @param  {[type]} req [request param]
 * @param  {[type]} res [response param]
 * @return {[type]}     [return list of comments or error]
 */
function getRecent(req, res) {
  let lang = req.header(LANG_HEADER);
  commentLocal.setLocale(lang);

  Comment.find()
    .sort({ updatedAt: 'desc' })
    .limit(5)
    .populate('author entry')
    .then((comments) => {
      res.send(comments);
    })
    .catch((err) => {
      res.send(err);
    });
}

/**
 * update comment
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
function update(req, res) {
  let commentId = req.params.id;
  let body = _.pick(req.body, ['text', 'author']);

  _checkId(res, commentId, 'There is no comment');

  Comment.findOneAndUpdate({ _id: commentId, author: body.author._id }, { $set: body }, { new: true })
    .then((comment) => {

      if(!comment) {
        return res.status(404).send({ message: commentLocal.translate('Comment is not found') })
      }

      res.send(comment);
    })
    .catch((err) => res.send(err));
}

/**
 * delete comment
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
function remove(req, res) {
  let commentId = req.params.commentId,
      entryId = req.params.entryId;

  _checkId(res, commentId, 'There is no comment');

  waterfall([
    // finde user profile
    (done) => {
      UserProfile.findOne({ _userId: req.user._id })
        .then((profile) => {

          done(null, profile);
        })
        .catch((err) => done(err));
    },
    // remove comment (or parent comment)
    (profile, done) => {
      let children = [];
      Comment.findOne({_id: commentId, author: profile._id})
        .then((comment) => {

          children = comment.children;
          children.push(comment._id);

          comment.remove()
            .then(() => {
              done(null, children);
            })
          .catch((err) => done(err));
        })
        .catch((err) => done(err));
    },
    (comments, done) => {

      Entry.findOneAndUpdate({ _id: entryId }, { $pull: { comments: { $in: comments}}})
        .then((entry) => {
          if(!entry) {
            return res.status(404).send({ message: entryLocal.translate('Entry is not found') });
          }

          res.status(200).send({ message: commentLocal.translate('Comment has been successfully removed') });
        })
        .catch((err) => done(err));
    }
  ], (err) => {
    res.send(err);
  });

}

function _checkId(res, id, message) {
  if (!ObjectID.isValid(id)) {
    return res.status(404).send({ message: commentLocal.translate(message) });
  }
}

export default { add, getListWithEntryId, getRecent, update, remove };
