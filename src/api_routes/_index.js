import category from './category_api';
import comment from './comment_api';
import entry from './entry_api';
import user from './user_api';
import profile from './userProfile_api';

export const categoryAPI = category;
export const commentAPI = comment;
export const entryAPI = entry;
export const userAPI = user;
export const profileAPI = profile;
