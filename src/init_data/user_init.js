import { User, UserProfile } from './../models/_index';
import { USER_ROLE } from './../configs/constants.config';

function createSuperUser() {
  let superAdmin = new User({
    email: 'vlad.ovinnikov@gmail.com',
    password: { hash: 'Vlad12345' },
    role: USER_ROLE.SUPER_ADMIN
  });
  User.findOne({ email: superAdmin.email })
    .then((superAdminExists) => {
      if(!superAdminExists) {

        superAdmin.save((err) => {
          if (err) {
            console.log(err);
          } else {
            console.log('SuperAdmin has successfully created!');
          }
        });

        let profile = new UserProfile({ _userId: superAdmin._id });
        profile.firstName = superAdmin.email.split('@')[0];
        profile.lastName = 'Doe';
        profile.save((err) => {
          if (err) {
            console.log(err);
          } else {
            console.log('SuperAdmins profile has successfully created!');
          }
        });
      } else {
        console.log('SuperAdmin with that email has already existed!');
      }
    })
    .catch( (err) => console.log(err) )
}

export default { createSuperUser };
