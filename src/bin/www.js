//-- Dependencies --//
// lib dependencies
import debug from 'debug';
import http from 'http';
// app dependencies
import app from './../app';
import dbConfig from './../configs/db.config';

// debug('cubic-house-backend:server');

//-- Implememtation --//
// Get port from environment and store in Express.
const PORT = normalizePort(process.env.PORT);
app.set('port', PORT);

// Create HTTP server.
let server = http.createServer(app);

// Listen on provided port, on all network interfaces.
server.listen(PORT, (err) => {
  if (err)throw err;

  console.log(`Express server listening on port ${PORT}`);
});
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 * @param  {[type]} val [description]
 * @return {[type]}     [description]
 */
function normalizePort(val) {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 * @param  {[type]} error [description]
 * @return {[type]}       [description]
 */
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof PORT === 'string' ? 'Pipe ' + PORT : 'Port ' + PORT;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use ${dbConfig}`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 * @return {[type]} [description]
 */
function onListening() {
  let addr = server.address();
  let bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  debug(`Listening on ${bind}`);
}
