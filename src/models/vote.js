//-- Dependencies --//
// lib dependencies
import mongoose from 'mongoose';
import moment from 'moment';

// app dependencies

const { Schema } = mongoose;

let VoteSchema = new Schema(
  {
    createdAt: { type: String, default: moment().format() },
    updatedAt: { type: String, default: moment().format() },
    author: { type: Schema.Types.ObjectId, ref: 'User' }
  }
);

// generate public JSON
VoteSchema.methods.toJSON = toJSON;

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  let vote = this;

  return {
    _id: vote._id,
    createdAt: vote.createdAt,
    updatedAt: vote.updatedAt,
    author: vote.author
  }
}

/**
 * Exports Comment model
 * @type {[type]}
 */
let Vote = mongoose.model('Vote', VoteSchema);
//-- Exports --//
export default Vote;
