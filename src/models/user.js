//-- Dependencies --//
// lib dependencies
import moment from 'moment';
import 'moment-timezone';
import mongoose from 'mongoose';
import _ from 'underscore';
import validator from 'validator';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import crypto from 'crypto';
import nodemailer from 'nodemailer';
// app dependencies
import { USER_ROLE } from './../configs/constants.config';
import { SALT_WORK_FACTOR } from './../configs/constants.config';
import { TZ_HEADER } from './../configs/constants.config';
import { userLocal } from './../assets/translations/translations';
import { MailGenerator } from './../assets/helpers/mailGenerator';

//-- Implementation --//
const { Schema } = mongoose;

let UserSchema = new Schema(
  {
    email: {
      type: String,
      unique: true,
      lowercase: true,
      required: 'Email is required',
      trim: true,
      minlength: 1,
      validate: {
        validator: validator.isEmail,
        message: 'Email is not valid'
      }
    },
    password: {
      hash: { type: String, required: true, trim: true, minlength: 6 },
      createdAt: { type: Date, default: moment().format() },
      updatedAt: { type: Date, default: moment().format() }
    },
    tokens: [
      {
        access: { type: String, require: true },
        token: { type: String, require: true },
        expiresIn: { type: Date }
      }
    ],
    role: {
      type: String,
      enum: [USER_ROLE.USER, USER_ROLE.ADMIN, USER_ROLE.SUPER_ADMIN],
      default: USER_ROLE.USER
    },
    updatedAt: { type: Date, default: moment().format() },
    createdAt: { type: Date, default: moment().format() }
  },
  {
    timestamps: { createdAt: 'createdAt' }
  }
);

// generate authentication token when user is creating
UserSchema.methods.generateAuthToken = generateAuthToken;
// find user by token
UserSchema.statics.findByToken = findByToken;
// find user by its credentials
UserSchema.statics.findByCredentials = findByCredentials;
// remove auth token
UserSchema.methods.removeToken = removeToken;
// generate public JSON
UserSchema.methods.toJSON = toJSON;
// hashing a password
UserSchema.pre('save', hashPassword);
UserSchema.pre('save', updateDate);

/**
 * implementation of generate authentication token when user is creating
 * @return {[type]} [return token to the user]
 */
function generateAuthToken(req) {
  let user = this;
  const access = 'auth';

  const expiresIn = moment().format();
  let token = jwt.sign({
    _id: user._id.toHexString(),
    expiresIn: expiresIn
  }, process.env.TOKEN_SECRET).toString();

  // add token to the user
  user.tokens.push({ access, token, expiresIn });

  return user.save().then(() => { return token; });
}

/**
 * implementation find user by token
 * @param  {[type]} token [token]
 * @return {[type]}       [return user if token verified or reject promise]
 */
function findByToken(token) {
  let User = this;
  let decoded;

  try {
    decoded = jwt.verify(token, process.env.TOKEN_SECRET);
  } catch (err) {
    return Promise.reject();
  }
  // TODO: add expiresIn for token
  // if(decoded) {
  //   return Promise.reject(`error1 ${moment().unix()}`);
  // }

  return User.findOne({
    _id: decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  })
  .then((user) => {
    return user;
  });
}
/**
 * [find user by its credentials
 * @param  {[type]} email    [description]
 * @param  {[type]} password [description]
 * @return {[type]}          [description]
 */
function findByCredentials(email, password, req, next) {
  let User = this;
  return User.findOne({
            email
          })
            .then((user) => {
              // if no user reject error
              if(!user) {
                return new Promise((resolve, reject) => { reject('Authentication failed. User does not exist')});
              }

              return new Promise((resolve, reject) => {
                bcrypt.compare(password, user.password.hash, (err, res) => {
                  // search token emailConfirm if found send error message
                  // if not success
                  _.find(user.tokens, (token) => {
                    if(token.access === 'emailConfirm') {
                      if(moment(token.expiresIn).isBefore(moment().format())) {
                        _randomBytes()
                          .then((newToken) => {
                            token.token = newToken;
                            token.expiresIn = moment().add(1, 'days');

                            user.save((err) => {
                              if(err) { reject(err); }
                              next();
                            });
                          })
                          .catch((err) => {
                            reject(err);
                          });
                      }
                      const smtpTransport = nodemailer.createTransport({
                        pool: true,
                        host: 'smtp.gmail.com',
                        port: 465,
                        secure: true,
                        auth: {
                          user: process.env.EMAIL,
                          pass: process.env.EMAIL_PASSWORD
                        }
                      });
                      let mailGenerator = new MailGenerator (
                        user.email,
                        process.env.EMAIL,
                        userLocal.translate('Cubic House: Request to confirm your account'),
                        `
                          <h2>${userLocal.translate('Hello')}</h2>
                          <h3>${userLocal.translate('Thank you for registration')}</h3>
                          <p>${userLocal.translate('To complete the registration process, please activate the Cubic House Blog profile by running the following activation link')}</p>
                          <p>
                            <span>Login: </span><span style="font-weight: bold">${user.email}</span>
                          </p>
                          <p style="width: 100%;
                                    padding: 30px;
                                    margin-bottom: 30px;
                                    background-color: #f0f5de;">
                            <a href="${req.headers.origin}/enter/email-confirm/${token.token}">${userLocal.translate('Confirm email')}</a>
                          </p>
                          <p style="font-weight: bold;">${userLocal.translate('Warning!')}</p>
                          <p>${userLocal.translate('The activation link is valid for 24 hours. After this time to activate the profile, please send an email to info.cubichouse@gmail.com with the information in the subject line \"Activation Cubic House Blog\"')}</p>
                        `
                      );
                      
                      smtpTransport.sendMail(mailOptions, (err) => {
                        if(err) { reject(err); }
                        next();
                      });
                      reject('Authentication failed. You can not login before email is confirmed. We have been sent to you a new email with confirmation link');
                    }
                  });

                  if(res) {
                    resolve(user);
                  } else {
                    reject('Authentication failed. Password is not correct');
                  }
                });
              });
            });
}

function removeToken(token) {
  let user = this;

  return user.update({
    $pull: {
      tokens: { token }
    }
  })
}

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  let user = this;

  return {
    _id: user._id,
    email: user.email,
    role: user.role,
    updatedAt: user.updatedAt,
    createdAt: user.createdAt,
    passwordUpdatedAt: user.password.updatedAt
  }
}
/**
 * update date
 * @param  {Function} next
 * @return {[type]}
 */
function updateDate(next) {
  let user = this;
  user.updatedAt = moment().format();
  next();
}

/**
 * generate hash for password recovering
 * @param  {Function} next
 * @return {[type]}
 */
function hashPassword(next) {
  let user = this;
  // user.updatedAt = moment().format();
  if(user.isModified('password') || user.isNew) {
    // user.createdAt = { type: String, default: moment().format() };
    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
      if (err) next(err);
      // hash the password along with our new salt
      bcrypt.hash(user.password.hash, salt, (err, hash) => {
        if (err) next(err);
        // override the cleartext password with the hashed one
        user.password.hash = hash;
        user.password.updatedAt = moment().format();
        next();
      });
    });
  } else {
    next();
  }
}


function _randomBytes() {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(20, (err, buf) => {
      if(buf) {
        let token = buf.toString('hex');
        resolve(token);
      } else {
        reject(err);
      }
    });
  })
}

/**
 * Exports User model
 * @type {[type]}
 */
let User = mongoose.model('User', UserSchema);

//-- Exports --//
export default User;
