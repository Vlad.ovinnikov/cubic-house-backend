//-- Dependencies --//
// lib dependencies
import mongoose from 'mongoose';
import moment from 'moment';
import _ from 'underscore';
// app dependencies
import { PRIORITY, STATUS } from './../configs/constants.config';
import { Comment } from './_index';

const { Schema } = mongoose;

let EntrySchema = new Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      minlength: 3
    },
    description: {
      type: String,
      required: true,
      trim: true,
      minlength: 10
    },
    priority: {
      type: Number,
      default: PRIORITY.NONE
    },
    status: {
      type: Number,
      default: STATUS.DRAFT
    },
    createdAt: { type: String, default: moment().format()} ,
    updatedAt: { type: String, default: moment().format() },
    publishedAt: { type: String, default: moment().format() },
    author: {
      type: Schema.Types.ObjectId,
      ref: 'UserProfile',
      required: true
    },
    category: {
      type: Schema.Types.ObjectId,
      ref: 'Category',
      required: true
    },
    votes: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Vote'
      }
    ],
    comments: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Comment'
      }
    ]
  }
);

// generate public JSON
EntrySchema.methods.toJSON = toJSON;
// update category date
EntrySchema.pre('save', updateDate);
EntrySchema.pre('remove', removeCascade);
/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  let entry = this;

  return {
    _id: entry._id,
    title: entry.title,
    description: entry.description,
    priority: entry.priority,
    status: entry.status,
    createdAt: entry.createdAt,
    updatedAt: entry.updatedAt,
    publishedAt: entry.publishedAt,
    author: entry.author,
    category: entry.category,
    comments: entry.comments
  }
}

/**
 * update date of category before save
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
function updateDate(next) {
  let entry = this;
  entry.updatedAt = moment().format();

  next();
}

function removeCascade(next) {
  Comment.remove({ entry: this._id }).exec();

  next();
}

/**
 * Exports Entry model
 * @type {[type]}
 */
let Entry = mongoose.model('Entry', EntrySchema);
//-- Exports --//
export default Entry;
