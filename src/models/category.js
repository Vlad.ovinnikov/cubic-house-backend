//-- Dependencies --//
// lib dependencies
import mongoose from 'mongoose';
import moment from 'moment';
import { ObjectID } from 'mongodb';
import _ from 'underscore';

import { Entry, Comment } from './_index';

// app dependencies
const { Schema } = mongoose;

let CategorySchema = new Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
      minlength: 3
    },
    description: {
      type: String,
      required: false
    },
    createdAt: { type: Date, default: moment().format() },
    updatedAt: { type: Date, default: moment().format() },
    publishedAt: { type: Date, default: moment().format() },
    author: { type: Schema.Types.ObjectId, ref: 'UserProfile' }
  }
);

// generate public JSON
CategorySchema.methods.toJSON = toJSON;
// update category date
CategorySchema.pre('save', updateDate);
CategorySchema.pre('remove', removeCascade);

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  let category = this;

  return {
    _id: category._id,
    title: category.title,
    description: category.description,
    updatedAt: category.updatedAt,
    createdAt: category.createdAt,
    publishedAt: category.publishedAt,
    author: category.author
  }
}

/**
 * update date of category before save
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
function updateDate(next) {
  let category = this;
  category.title = category.title[0].toUpperCase() + category.title.slice(1);
  category.updatedAt = moment().format();

  next();
}

/**
 * remove cascade
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
function removeCascade(next) {

  Entry.find({ category: this._id })
    .exec((err, entries)=> {
      _.each(entries, (entry) => {
        Entry.remove({_id: entry._id}).exec()
          .then((res) => Comment.remove({ entry: entry._id }).exec());
      });
    });

  next();
}

/**
 * Exports Category model
 * @type {[type]}
 */
let Category = mongoose.model('Category', CategorySchema);

//-- Exports --//
export default Category;
