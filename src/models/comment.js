//-- Dependencies --//
// lib dependencies
import mongoose from 'mongoose';
import moment from 'moment';

// app dependencies

const { Schema } = mongoose;

let CommentSchema = new Schema(
  {
    text: {
      type: String,
      required: true,
      trim: true,
      minlength: 3
    },
    parentId: {
      type: Schema.Types.ObjectId,
      ref: 'Comment',
      default: null
    },
    children: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Comment'
      }
    ],
    votes: [
      {
        type: Schema.Types.ObjectId,
        ref: 'UserProfile'
      }
    ],
    createdAt: { type: String, default: moment().format() },
    updatedAt: { type: String, default: moment().format() },
    author: {
      type: Schema.Types.ObjectId,
      ref: 'UserProfile'
    },
    entry: {
      type: Schema.Types.ObjectId,
      ref: 'Entry'
    }
  }
);

// generate public JSON
CommentSchema.methods.toJSON = toJSON;
// update comment date
CommentSchema.pre('save', updateDate);
CommentSchema.pre('remove', removeCascade);

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  let comment = this;

  return {
    _id: comment._id,
    text: comment.text,
    votes: comment.votes,
    parentId: comment.parentId,
    createdAt: comment.createdAt,
    updatedAt: comment.updatedAt,
    author: comment.author,
    entry: comment.entry,
    children: comment.children
  }
}

/**
 * update date of comment before save
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
function updateDate(next) {
  let comment = this;
  comment.updatedAt = moment().format();

  next();
}

/**
 * remove nested comments
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
function removeCascade(next) {
  Comment.remove({ parentId: this._id }).exec();

  next();
}

/**
 * Exports Comment model
 * @type {[type]}
 */
let Comment = mongoose.model('Comment', CommentSchema);
//-- Exports --//
export default Comment;
