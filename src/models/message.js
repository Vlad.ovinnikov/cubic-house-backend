//-- Dependencies --//
// lib dependencies
import mongoose from 'mongoose';
import moment from 'moment';

// app dependencies

const { Schema } = mongoose;

let MessageSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
      minlength: 3
    },
    description: {
      type: String,
      required: true,
      minlength: 10
    },
    priority: {
      type: Number,
      required: true,
      default: 0
    },
    createdAt: { type: String, default: moment().format() },
    updatedAt: { type: String, default: moment().format() },
    sender: {type: Schema.Types.ObjectId, ref: 'User'},
    receiver: {type: Schema.Types.ObjectId, ref: 'Entry'}
  }
);

// generate public JSON
MessageSchema.methods.toJSON = toJSON;

/**
 * implementation of generate public JSON
 * @return {[type]} public user JSON
 */
function toJSON() {
  let message = this;

  return {
    _id: message._id,
    title: message.title,
    description: message.description,
    priority: message.priority,
    createdAt: message.createdAt,
    updatedAt: message.updatedAt,
    sender: message.sender,
    receiver: message.receiver
  }
}

/**
 * Exports Message model
 * @type {[type]}
 */
let Message = mongoose.model('Message', MessageSchema);
//-- Exports --//
export default Message;
